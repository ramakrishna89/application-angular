import {Component, OnInit} from '@angular/core';
import {LoggerService} from '../shared/utils/logger.service';
import {AlertService} from '../shared/services/alert.service';
import {SessionService} from '../shared/services/session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {


  constructor(private sessionService: SessionService,
              private logger: LoggerService, private alertService: AlertService) {
  }

  ngOnInit(): void {
    this.initializeDashboard();
  }

  initializeDashboard(): void {
    this.alertService.info('Welcome ' + this.sessionService.currentUserValue.name + '!!!', false);
  }
}
