import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {APP_NAVIGATION_CONSTANTS} from '../shared/constants/app.constant';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_HOME + '.name',
      headerDisplay: 'none',
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
