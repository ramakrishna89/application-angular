import {NgModule} from '@angular/core';
import {DownloadsComponent} from './index/downloads.component';
import {SharedModule} from '../shared/shared.module';
import {DownloadsRouteModule} from './route/downloads-route.module';
import {DownloadsListComponent} from './list/downloads-list.component';


@NgModule({
  declarations: [DownloadsComponent, DownloadsListComponent],
  imports: [
    SharedModule,
    DownloadsRouteModule
  ]
})
export class DownloadsModule {
}
