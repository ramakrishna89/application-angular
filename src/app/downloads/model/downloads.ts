import {Audit} from '../../../shared/interfaces/audit.type';

export interface Downloads extends Audit {
  name: string;
  description: string;
}
