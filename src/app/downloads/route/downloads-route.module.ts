import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DownloadsComponent} from '../index/downloads.component';
import {DownloadsListComponent} from '../list/downloads-list.component';
import {APP_NAVIGATION_CONSTANTS} from '../../shared/constants/app.constant';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_DOWNLOADS,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_DOWNLOADS
};

export const routes: Routes = [
  {
    path: '',
    component: DownloadsComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_DOWNLOADS + '.name',
      description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_DOWNLOADS + '.description',
      icon: 'cloud-download',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: DownloadsListComponent,
        data: {
          title: 'app.buttons.all',
          ...pageNameAndEndPoint
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DownloadsRouteModule {
}
