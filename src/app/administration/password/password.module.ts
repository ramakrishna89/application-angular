import {NgModule} from '@angular/core';
import {PasswordComponent} from './index/password.component';
import {PasswordOpsComponent} from './ops/password-ops.component';
import {SharedModule} from '../../shared/shared.module';
import {PasswordRouteModule} from './route/password-route.module';


@NgModule({
  declarations: [PasswordComponent, PasswordOpsComponent],
  imports: [
    PasswordRouteModule,
    SharedModule
  ]
})
export class PasswordModule {
}
