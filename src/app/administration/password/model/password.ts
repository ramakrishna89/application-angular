import {GenericModelType} from '../../../shared/interfaces/generic-model.type';

export interface Password extends GenericModelType {
  oldPassword?: string;
  password?: string;
  confirmPassword?: string;
}
