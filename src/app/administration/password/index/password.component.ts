import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styles: []
})
export class PasswordComponent implements OnInit {

  constructor(public acRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

}
