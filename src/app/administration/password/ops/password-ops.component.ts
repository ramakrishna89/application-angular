import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';
import {Password} from '../model/password';
import {SessionService} from '../../../shared/services/session.service';

@Component({
  selector: 'app-password-ops',
  templateUrl: './password-ops.component.html',
  styles: []
})
export class PasswordOpsComponent extends OpsAbstract implements OnInit {

  constructor(public fb: FormBuilder,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              public acService: AppComponentService,
              public alertService: AlertService,
              public service: CrudService,
              private sessionService: SessionService
  ) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.curdForm = this.fb.group({
      email: [this.sessionService.currentUserValue.email, []],
      name: [this.sessionService.currentUserValue.email, []],
      oldPassword: this.acService.generateFormControl(AppComponentsEnum.PASSWORD, this.object?.oPassword),
      password: this.acService.generateFormControl(AppComponentsEnum.PASSWORD, this.object?.password),
      confirmPassword: this.acService.generateFormControl(AppComponentsEnum.PASSWORD, this.object?.cPassword),
    });
  }

  customCreateValidations(): boolean {
    return true;
  }

  customUpdateValidations(): boolean {
    if (this.curdForm && this.curdForm.valid) {
      const formObj: Password = this.curdForm.value;
      if (formObj.password !== formObj.confirmPassword) {
        this.alertService.error('app.error.msg-25', true);
      } else if (formObj.password.indexOf('@') <= 0 && formObj.password.indexOf('#') <= 0 &&
        formObj.password.indexOf('$') <= 0) {
        this.alertService.error('app.error.msg-24', true);
      } else {
        return true;
      }
    }
    return false;
  }

  customPostFailureOps(): void {
  }

  customPostSuccessOps(): void {
  }
}
