import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {PasswordComponent} from '../index/password.component';
import {PasswordOpsComponent} from '../ops/password-ops.component';

const pageNameAndEndPoint: any = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_PASSWORD,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_PASSWORD
};

export const routes: Routes = [
  {
    path: '',
    component: PasswordComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_PASSWORD + '.name',
      description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_PASSWORD + '.description',
      icon: 'key',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: PasswordOpsComponent,
        data: {
          title: 'app.buttons.add',
          curdOps: APP_CURD_CONSTANTS.UPDATE,
          ...pageNameAndEndPoint
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasswordRouteModule {
}
