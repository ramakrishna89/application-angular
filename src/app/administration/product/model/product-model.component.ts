import {GenericModelType} from '../../../shared/interfaces/generic-model.type';

export interface Product extends GenericModelType {

  code: string;
}
