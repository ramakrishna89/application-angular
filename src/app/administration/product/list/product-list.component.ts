import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ListAbstract} from '../../../shared/services/abstract/list.abstract';
import {CrudService} from '../../../shared/services/crud.service';
import {AlertService} from '../../../shared/services/alert.service';
import {SessionService} from '../../../shared/services/session.service';

@Component({selector: 'app-product-list', templateUrl: './product-list.component.html', styles: []})
export class ProductListComponent extends ListAbstract implements OnInit {
  constructor(protected service: CrudService, protected alertService: AlertService,
              protected activatedRoute: ActivatedRoute, private sessionService: SessionService) {
    super(activatedRoute, service, alertService);
  }

  ngOnInit(): void {
    super.init();
  }
}
