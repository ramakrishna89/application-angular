import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductIndexComponent} from '../index/product-index.component';
import {ProductListComponent} from '../list/product-list.component';
import {ProductOpsComponent} from '../ops/product-ops.component';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_PRODUCT,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_PRODUCT
};
export const routes: Routes = [{
  path: '',
  component: ProductIndexComponent,
  data: {
    title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_PRODUCT + '.name',
    description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_PRODUCT + '.description',
    icon: 'menu', ...pageNameAndEndPoint
  },
  children: [{
    path: '',
    component: ProductListComponent,
    data: {title: 'app.buttons.all', ...pageNameAndEndPoint}
  }, {
    path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
    component: ProductOpsComponent,
    data: {title: 'app.buttons.add', curdOps: APP_CURD_CONSTANTS.CREATE, ...pageNameAndEndPoint}
  }, {
    path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
    component: ProductOpsComponent,
    data: {title: 'app.buttons.view', curdOps: APP_CURD_CONSTANTS.READ, ...pageNameAndEndPoint},
    resolve: {object: RouteResolver},
  }, {
    path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
    component: ProductOpsComponent,
    data: {title: 'app.buttons.edit', curdOps: APP_CURD_CONSTANTS.UPDATE, ...pageNameAndEndPoint},
    resolve: {object: RouteResolver},
  }]
}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class ProductRouteModule {
}
