import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({selector: 'app-product-index', templateUrl: './product-index.component.html', styles: []})
export class ProductIndexComponent implements OnInit {
  constructor(public acRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
  }
}
