import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';

@Component({selector: 'app-product-ops', templateUrl: './product-ops.component.html', styles: []})
export class ProductOpsComponent extends OpsAbstract implements OnInit {
  constructor(public fb: FormBuilder, public router: Router, public activatedRoute: ActivatedRoute,
              public acService: AppComponentService, public alertService: AlertService,
              public service: CrudService, public translate: TranslateService) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.curdForm = this.fb.group({
      code: this.acService.generateFormControl(AppComponentsEnum.BASIC, this.object?.code),
    });
  }

  customCreateValidations(): boolean {
    return true;
  }

  customPostFailureOps(): void {
  }

  customPostSuccessOps(): void {
  }

  customUpdateValidations(): boolean {
    return true;
  }
}
