import {NgModule} from '@angular/core';
import {ProductIndexComponent} from './index/product-index.component';
import {ProductListComponent} from './list/product-list.component';
import {ProductOpsComponent} from './ops/product-ops.component';
import {ProductRouteModule} from './route/product-route.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [ProductIndexComponent, ProductListComponent, ProductOpsComponent],
  imports: [SharedModule, ProductRouteModule]
})
export class ProductModule {
}
