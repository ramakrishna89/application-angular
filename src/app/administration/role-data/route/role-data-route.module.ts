import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RoleDataComponent} from '../index/role-data.component';
import {RoleDataListComponent} from '../list/role-data-list.component';
import {RoleDataOpsComponent} from '../ops/role-data-ops.component';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ROLE_DATA,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_ROLE_DATA
};

export const routes: Routes = [
  {
    path: '',
    component: RoleDataComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_ROLE_DATA + '.name',
      description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_ROLE_DATA + '.description',
      icon: 'menu',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: RoleDataListComponent,
        data: {
          title: 'app.buttons.all',
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
        component: RoleDataOpsComponent,
        data: {
          title: 'app.buttons.add',
          curdOps: APP_CURD_CONSTANTS.CREATE,
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
        component: RoleDataOpsComponent,
        data: {
          title: 'app.buttons.view',
          curdOps: APP_CURD_CONSTANTS.READ,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
        component: RoleDataOpsComponent,
        data: {
          title: 'app.buttons.edit',
          curdOps: APP_CURD_CONSTANTS.UPDATE,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleDataRouteModule {
}
