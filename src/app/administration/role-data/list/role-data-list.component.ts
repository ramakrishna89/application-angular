import {Component, OnInit} from '@angular/core';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';
import {ListAbstract} from '../../../shared/services/abstract/list.abstract';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-role-data-list',
  templateUrl: './role-data-list.component.html',
  styles: []
})
export class RoleDataListComponent extends ListAbstract implements OnInit {

  constructor(protected activatedRoute: ActivatedRoute, protected service: CrudService, protected alertService: AlertService) {
    super(activatedRoute, service, alertService);
  }

  ngOnInit(): void {
    super.init();
  }

}
