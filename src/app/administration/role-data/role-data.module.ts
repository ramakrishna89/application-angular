import {NgModule} from '@angular/core';
import {RoleDataComponent} from './index/role-data.component';
import {RoleDataListComponent} from './list/role-data-list.component';
import {RoleDataOpsComponent} from './ops/role-data-ops.component';
import {RoleDataRouteModule} from './route/role-data-route.module';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [RoleDataComponent, RoleDataListComponent, RoleDataOpsComponent],
  imports: [
    SharedModule,
    RoleDataRouteModule
  ]
})
export class RoleDataModule {
}
