import {GenericModelType} from '../../../shared/interfaces/generic-model.type';
import {IDBDetails} from './db-details';

export interface RoleData extends GenericModelType {
  code: string;
  dbDetailList: IDBDetails[];
}
