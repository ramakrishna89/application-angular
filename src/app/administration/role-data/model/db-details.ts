import {GenericModelType} from '../../../shared/interfaces/generic-model.type';

export interface IDBDetails extends GenericModelType {

  index: number;
  dataId: string;
  driver: string;
  url: string;
  userName: string;
  password: string;
  productId: number;
  roleDataId: number;
}

export class DBDetails implements IDBDetails {

  index: number;
  dataId: string;
  description: string;
  driver: string;
  id: number;
  name: string;
  password: string;
  productId: number;
  roleDataId: number;
  url: string;
  userName: string;

  constructor(index: number, driver: string, name: string, password: string, url: string, userName: string) {
    this.index = index;
    this.driver = driver;
    this.name = name;
    this.password = password;
    this.url = url;
    this.userName = userName;
  }

}
