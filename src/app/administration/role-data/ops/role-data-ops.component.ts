import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';
import {DBDetails, IDBDetails} from '../model/db-details';
import {RoleData} from '../model/role-data';
import {updateFormDirtyAndValueAndValidity} from '../../../shared/utils/Utils.functions';
import {APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';

@Component({
  selector: 'app-role-data-ops',
  templateUrl: './role-data-ops.component.html',
  styles: []
})
export class RoleDataOpsComponent extends OpsAbstract implements OnInit {


  listOfDbDetails: Array<IDBDetails> = [];
  productIdNames: Map<string, string>;

  constructor(public fb: FormBuilder,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              public acService: AppComponentService,
              public alertService: AlertService,
              public service: CrudService
  ) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.getProductIdNameMapping();
    this.curdForm = this.fb.group({
      code: this.acService.generateFormControl(AppComponentsEnum.BASIC, this.object?.code),
      dbDetailList: this.fb.array([]),
    });
  }

  customCreateValidations(): boolean {
    return true;
  }

  customUpdateValidations(): boolean {
    return true;
  }

  customCreateOrUpdate(): void {
    console.log(this.curdForm.value);
    updateFormDirtyAndValueAndValidity(this.curdForm);
    if (this.curdForm.valid) {
      const roleData: RoleData = this.curdForm.value;
      if (this.listOfDbDetails.length > 0) {
        roleData.dbDetailList = this.listOfDbDetails;
      }
      this.createOrUpdate(roleData);
    } else {
      console.log('Invalid Control: ', this.findInvalidControls());
    }
  }

  customPostFailureOps(): void {
  }

  customPostSuccessOps(): void {
    this.listOfDbDetails = [];
  }

  addDBDetail(e?: MouseEvent): void {
    if (e) {
      e.preventDefault();
    }
    const index = this.listOfDbDetails.length > 0 ? this.listOfDbDetails[this.listOfDbDetails.length - 1].id + 1 : 0;
    this.listOfDbDetails = [
      ...this.listOfDbDetails,
      new DBDetails(index, '', `DbDetail${index}`, '', '', '')
    ];
    console.log(this.listOfDbDetails[this.listOfDbDetails.length - 1]);
    this.curdForm.addControl(`product${index}`, this.acService.generateFormControl(AppComponentsEnum.SELECT, '', 1));
    this.curdForm.addControl(`driver${index}`, this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1));
    this.curdForm.addControl(`url${index}`, this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1));
    this.curdForm.addControl(`username${index}`, this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1));
    this.curdForm.addControl(`password${index}`, this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1));
  }

  deleteDBDetail(index: number): void {
    this.listOfDbDetails = this.listOfDbDetails.filter(d => d.index !== index);
    this.curdForm.removeControl(`product${index}`);
    this.curdForm.removeControl(`driver${index}`);
    this.curdForm.removeControl(`url${index}`);
    this.curdForm.removeControl(`username${index}`);
    this.curdForm.removeControl(`password${index}`);
  }

  addDbDetailToFromGroup(e?: MouseEvent): void {
    if (e) {
      e.preventDefault();
    }
    (this.curdForm.get('dbDetailList') as FormArray).push(new FormGroup({
      product: this.acService.generateFormControl(AppComponentsEnum.SELECT, '', 1),
      driver: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1),
      url: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1),
      username: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1),
      password: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, '', 1)
    }));
  }

  deleteDbDetailFromFromGroup(i: number): void {
    const dbDetailList = this.curdForm.get('dbDetailList') as FormArray;
    if (dbDetailList.length > 0) {
      dbDetailList.removeAt(i);
    } else {
      dbDetailList.reset();
    }
  }

  getProductIdNameMapping(): void {
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_PRODUCT + APP_NAVIGATION_CONSTANTS.API.SERVICE_ID_NAME_MAP)
      .subscribe(idNames => {
        this.productIdNames = idNames;
      });
  }

}
