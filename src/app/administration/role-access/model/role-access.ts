import {GenericModelType} from '../../../shared/interfaces/generic-model.type';

export interface RoleAccess extends GenericModelType {
  selectedMenuIds?: number[];
  selectedRoleDataIds?: number[];
  selectedProductIds?: number[];
}

