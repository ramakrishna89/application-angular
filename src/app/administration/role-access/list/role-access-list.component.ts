import {Component, OnInit} from '@angular/core';
import {CrudService} from '../../../shared/services/crud.service';
import {AlertService} from '../../../shared/services/alert.service';
import {APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {ListAbstract} from '../../../shared/services/abstract/list.abstract';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-access-list.component.html',
  styles: []
})
export class RoleAccessListComponent extends ListAbstract implements OnInit {

  constructor(protected activatedRoute: ActivatedRoute, protected service: CrudService, protected alertService: AlertService) {
    super(activatedRoute, service, alertService);
  }

  ngOnInit(): void {
    super.init();
  }


}
