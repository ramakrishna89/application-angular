import {NgModule} from '@angular/core';
import {RoleAccessComponent} from './index/role-access.component';
import {RoleAccessListComponent} from './list/role-access-list.component';
import {RoleAccessOpsComponent} from './ops/role-access-ops.component';
import {SharedModule} from '../../shared/shared.module';
import {RoleAccessRouteModule} from './route/role-access-route.module';
import {NzTabsModule} from 'ng-zorro-antd/tabs';
import {NzTreeModule} from 'ng-zorro-antd/tree';
import {NzTreeViewModule} from 'ng-zorro-antd/tree-view';


const antdModule = [
  NzTabsModule,
  NzTreeModule,
  NzTreeViewModule
];

@NgModule({
  declarations: [RoleAccessComponent, RoleAccessListComponent, RoleAccessOpsComponent],
  imports: [
    SharedModule,
    RoleAccessRouteModule,
    ...antdModule
  ]
})
export class RoleAccessModule {
}
