import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';
import {RoleAccess} from '../model/role-access';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {NzFormatEmitEvent, NzTreeNodeOptions} from 'ng-zorro-antd/tree';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-role-ops',
  templateUrl: './role-access-ops.component.html',
  styles: []
})
export class RoleAccessOpsComponent extends OpsAbstract implements OnInit {

  allMenuTypes: string[] | null;
  listOfNodes: ReadonlyArray<NzTreeNodeOptions> = [];
  setOfCheckedId = new Set<number>();
  selectedRoleDataIds: string[] = [];
  selectedProductIds: string[] = [];
  selectedTab = 0;
  private roleDataIdNames: Map<any, any>;
  private productIdNames: Map<any, any>;

  constructor(public fb: FormBuilder,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              public acService: AppComponentService,
              public alertService: AlertService,
              public service: CrudService,
              public translate: TranslateService
  ) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.getRoleDataIdNameMapping();
    this.getProductIdNameMapping();
    this.curdForm = this.fb.group({
      selectedRoleDataIds: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.selectedRoleDataIds),
      selectedProductIds: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.selectedRoleDataIds)
    });
    if (this.object && this.object.selectedMenuIds && this.object.selectedMenuIds.length > 0) {
      this.setOfCheckedId = new Set<number>(this.object.selectedMenuIds);
    }
    if (this.object && this.object.selectedRoleDataIds && this.object.selectedRoleDataIds.length > 0) {
      for (const id of this.object.selectedRoleDataIds) {
        this.selectedRoleDataIds.push(id + '');
      }
    }
    if (this.object && this.object.selectedProductIds && this.object.selectedProductIds.length > 0) {
      for (const id of this.object.selectedProductIds) {
        this.selectedProductIds.push(id + '');
      }
    }
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_MENU + 'load-all-menu-types').subscribe(data => {
      this.allMenuTypes = data;
      this.onTabClicked(this.allMenuTypes[0]);
    });
  }

  onTabClicked(menuType): void {
    this.isLoading = true;
    this.listOfNodes = [];
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_MENU + 'load-menus-of-menu-type', [{key: 'menuType', value: menuType}])
      .subscribe(data => {
        if (data) {
          data.forEach(node => {
            node.title = this.translate.instant('app.' + node.title?.toLowerCase() + '.name');
            if (node.children && node.children.length > 0) {
              node.children?.forEach(child => {
                child.title = this.translate.instant('app.' + child.title?.toLowerCase() + '.name');
                if (this.setOfCheckedId?.has(child.key)) {
                  child.checked = true;
                }
              });
            } else {
              if (this.setOfCheckedId?.has(node.key)) {
                node.checked = true;
              }
            }
          });
          this.listOfNodes = data;
        }
      });
    this.isLoading = false;
  }

  nzEvent(event: NzFormatEmitEvent): void {
    if (event?.node?.origin) {
      if (event.node.origin.checked) {
        this.setOfCheckedId.add(Number(event.node.origin.key));
        if (Number(event.node.origin.parentKey) !== 0) {
          this.setOfCheckedId.add(Number(event.node.origin.parentKey));
        }
        event.node.origin.children?.forEach(child => {
          if (child.checked) {
            this.setOfCheckedId.add(Number(child.key));
            if (Number(event.node.origin.parentKey) !== 0) {
              this.setOfCheckedId.add(Number(child.parentKey));
            }
          }
        });
      } else {
        this.setOfCheckedId.delete(Number(event.node.origin.key));
        event.node.origin.children?.forEach(child => {
          if (!child.checked) {
            this.setOfCheckedId.delete(Number(child.key));
          }
        });
        let anyNodeChecked = 0;
        event.node.parentNode?.origin.children.forEach(child => {
          if (child.checked) {
            anyNodeChecked++;
          }
        });
        if (anyNodeChecked === 0) {
          this.setOfCheckedId.delete(Number(event.node.parentNode.key));
        }
      }
      // console.log(this.setOfCheckedId);
      // console.log(event);
    }
  }

  customCreateOrUpdate(): void {
    if (this.curdForm.valid) {
      if (this.selectedRoleDataIds.indexOf('1') > -1 && this.selectedRoleDataIds.length > 1) {
        this.alertService.error('app.error.msg-33', true);
      } else {
        const roleAccess: RoleAccess = this.curdForm.value;
        roleAccess.selectedMenuIds = Array.from(this.setOfCheckedId.values());
        this.createOrUpdate(roleAccess);
      }
    }
  }

  resetTree(): void {
    this.setOfCheckedId.clear();
    this.selectedTab = 0;
    this.onTabClicked(this.allMenuTypes[0]);
  }

  customPostSuccessOps(): void {
    if (this.curdOps !== APP_CURD_CONSTANTS.UPDATE) {
      this.resetTree();
    }
  }

  customPostFailureOps(): void {
    // this.resetTree();
  }

  customCreateValidations(): boolean {
    return true;
  }

  customUpdateValidations(): boolean {
    return true;
  }

  getRoleDataIdNameMapping(): void {
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_ROLE_DATA + APP_NAVIGATION_CONSTANTS.API.SERVICE_ID_NAME_MAP)
      .subscribe(roleDataIdNames => {
        this.roleDataIdNames = roleDataIdNames;
      });
  }

  getProductIdNameMapping(): void {
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_PRODUCT + APP_NAVIGATION_CONSTANTS.API.SERVICE_ID_NAME_MAP)
      .subscribe(productIdNames => {
        this.productIdNames = productIdNames;
      });
  }


}
