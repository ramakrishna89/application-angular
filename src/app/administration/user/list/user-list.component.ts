import {Component, OnInit} from '@angular/core';
import {ListAbstract} from '../../../shared/services/abstract/list.abstract';
import {CrudService} from '../../../shared/services/crud.service';
import {AlertService} from '../../../shared/services/alert.service';
import {SessionService} from '../../../shared/services/session.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styles: []
})
export class UserListComponent extends ListAbstract implements OnInit {

  sessionUserEmail: string | null;

  constructor(protected service: CrudService,
              protected alertService: AlertService,
              protected activatedRoute: ActivatedRoute,
              private sessionService: SessionService) {
    super(activatedRoute, service, alertService);
  }

  ngOnInit(): void {
    this.sessionUserEmail = this.sessionService.currentUserValue.email;
    super.init();
  }

}
