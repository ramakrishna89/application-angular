import {Component, OnInit} from '@angular/core';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';
import {TranslateService} from '@ngx-translate/core';
import {APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';

@Component({
  selector: 'app-user-ops',
  templateUrl: './user-ops.component.html',
  styles: []
})
export class UserOpsComponent extends OpsAbstract implements OnInit {

  private roleAccessIdNames: Map<string, string>;

  constructor(public fb: FormBuilder,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              public acService: AppComponentService,
              public alertService: AlertService,
              public service: CrudService,
              public translate: TranslateService
  ) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.getRoleAccessIdNameMapping();
    this.curdForm = this.fb.group({
      address: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL_OPTIONAL, this.object?.address),
      email: this.acService.generateFormControl(AppComponentsEnum.EMAIL, this.object?.email),
      designation: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, this.object?.designation),
      phonePrefix: this.acService.generateFormControl(AppComponentsEnum.SELECT,
        (this.object && this.object.phonePrefix) ? this.object.phonePrefix : '+968'),
      phone: this.acService.generateFormControl(AppComponentsEnum.PHONE, this.object?.phone),
      lang: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.lang),
      selectedRoleAccessId: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.selectedRoleAccessId)
    });
  }


  getRoleAccessIdNameMapping(): void {
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_ROLE_ACCESS + APP_NAVIGATION_CONSTANTS.API.SERVICE_ID_NAME_MAP)
      .subscribe(roleAccessIdNames => {
        this.roleAccessIdNames = roleAccessIdNames;
      });
  }

  customCreateValidations(): boolean {
    return true;
  }

  customPostFailureOps(): void {
  }

  customPostSuccessOps(): void {
  }

  customUpdateValidations(): boolean {
    return true;
  }

}
