import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';
import {UserComponent} from '../index/user.component';
import {UserListComponent} from '../list/user-list.component';
import {UserOpsComponent} from '../ops/user-ops.component';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_USER,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_USER
};

export const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_USER + '.name',
      description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_USER + '.description',
      icon: 'menu',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: UserListComponent,
        data: {
          title: 'app.buttons.all',
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
        component: UserOpsComponent,
        data: {
          title: 'app.buttons.add',
          curdOps: APP_CURD_CONSTANTS.CREATE,
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
        component: UserOpsComponent,
        data: {
          title: 'app.buttons.view',
          curdOps: APP_CURD_CONSTANTS.READ,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
        component: UserOpsComponent,
        data: {
          title: 'app.buttons.edit',
          curdOps: APP_CURD_CONSTANTS.UPDATE,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRouteModule {
}
