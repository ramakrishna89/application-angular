import {RoleAccess} from '../../role-access/model/role-access';

export interface Auth {
  email: string;
  name: string;
  password: string;
  token?: string;
  refreshToken?: string;
  expiresAt?: Date;
  permission?: number;
  appPermission?: number;
  roleAccessDto?: RoleAccess;

}

