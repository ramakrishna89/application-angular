import {GenericModelType} from '../../../shared/interfaces/generic-model.type';

export interface User extends GenericModelType {
  email: string;
  phonePrefix: string;
  phone: string;
  address: string;
  designation: string;
  lang: string;
  selectedRoleAccessId: string;
}
