import {NgModule} from '@angular/core';
import {UserComponent} from './index/user.component';
import {UserListComponent} from './list/user-list.component';
import {UserOpsComponent} from './ops/user-ops.component';
import {SharedModule} from '../../shared/shared.module';
import {UserRouteModule} from './route/user-route.module';


@NgModule({
  declarations: [UserComponent, UserListComponent, UserOpsComponent],
  imports: [
    SharedModule,
    UserRouteModule
  ]
})
export class UserModule {
}
