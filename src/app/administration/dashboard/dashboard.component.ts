import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../shared/services/session.service';
import {AlertService} from '../../shared/services/alert.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})

export class DashboardComponent implements OnInit {


  constructor(private sessionService: SessionService, private alertService: AlertService) {
  }

  ngOnInit(): void {
    this.initializeDashboard();
  }

  initializeDashboard(): void {
    this.alertService.info('Hi ' + this.sessionService.currentUserValue.name + ', Dashboard is under construction.!!!',
      false);
  }
}
