import {GenericModelType} from '../../../shared/interfaces/generic-model.type';

export interface IMenu extends GenericModelType {
  path?: string;
  iconType?: string;
  iconTheme?: string;
  icon?: string;
  rootMenuId?: number;
  menuType?: string;
  submenu?: any[];
}




