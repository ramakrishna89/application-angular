import {Component, OnInit} from '@angular/core';
import {FormBuilder, ValidationErrors} from '@angular/forms';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {APP_ROLES_MENU_TYPES} from '../../../shared/constants/app.constant';
import {CrudService} from '../../../shared/services/crud.service';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';

@Component({
  selector: 'app-menu-ops',
  templateUrl: './menu-ops.component.html'
})
export class MenuOpsComponent extends OpsAbstract implements OnInit {

  roleMenuType: string[] = APP_ROLES_MENU_TYPES;

  constructor(public fb: FormBuilder,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              public acService: AppComponentService,
              public alertService: AlertService,
              public service: CrudService
  ) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.curdForm = this.fb.group({
      path: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, this.object?.path),
      iconType: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.iconType),
      iconTheme: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.iconTheme),
      menuType: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.menuType),
      icon: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, this.object?.icon),
      rootMenuId: this.acService.generateFormControl(AppComponentsEnum.NUMERIC, this.object?.rootMenuId)
    });
  }

  customCreateValidations(): boolean {
    return true;
  }


  customUpdateValidations(): boolean {
    return true;
  }

  customPostFailureOps(): void {
  }

  customPostSuccessOps(): void {
  }

}

