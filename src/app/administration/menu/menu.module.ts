import {NgModule} from '@angular/core';
import {MenuComponent} from './index/menu.component';
import {ListComponent} from './list/list.component';
import {MenuRouteModule} from './route/menu-route.module';
import {SharedModule} from '../../shared/shared.module';
import {MenuOpsComponent} from './ops/menu-ops.component';

@NgModule({
  declarations: [
    MenuComponent,
    ListComponent,
    MenuOpsComponent
  ],
  imports: [
    SharedModule,
    MenuRouteModule,
  ]
})
export class MenuModule {
}
