import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from '../list/list.component';
import {MenuComponent} from '../index/menu.component';
import {MenuOpsComponent} from '../ops/menu-ops.component';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_MENU,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_MENU
};

export const routes: Routes = [
  {
    path: '',
    component: MenuComponent,
    data: {
      title: 'app.menu-page.name',
      description: 'app.menu-page.description',
      icon: 'menu',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'app.buttons.all',
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
        component: MenuOpsComponent,
        data: {
          title: 'app.buttons.add',
          curdOps: APP_CURD_CONSTANTS.CREATE,
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
        component: MenuOpsComponent,
        data: {
          title: 'app.buttons.view',
          curdOps: APP_CURD_CONSTANTS.READ,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
        component: MenuOpsComponent,
        data: {
          title: 'app.buttons.edit',
          curdOps: APP_CURD_CONSTANTS.UPDATE,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRouteModule {
}
