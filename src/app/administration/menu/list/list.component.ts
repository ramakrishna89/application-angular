import {Component, OnInit} from '@angular/core';
import {AlertService} from '../../../shared/services/alert.service';
import {ListAbstract} from '../../../shared/services/abstract/list.abstract';
import {CrudService} from '../../../shared/services/crud.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent extends ListAbstract implements OnInit {

  constructor(protected activatedRoute: ActivatedRoute, protected service: CrudService, protected alertService: AlertService) {
    super(activatedRoute, service, alertService);
  }

  ngOnInit(): void {
    super.init();
  }

}
