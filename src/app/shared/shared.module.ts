import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ThemeConstantService} from './services/theme-constant.service';
import {SearchPipe} from './pipes/search.pipe';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {TranslateModule} from '@ngx-translate/core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {JwtInterceptor} from './interceptor/token.interceptor';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzTableModule} from 'ng-zorro-antd/table';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzPageHeaderModule} from 'ng-zorro-antd/page-header';
import {NzAvatarModule} from 'ng-zorro-antd/avatar';
import {NzEmptyModule} from 'ng-zorro-antd/empty';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzGridModule} from 'ng-zorro-antd/grid';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzSpaceModule} from 'ng-zorro-antd/space';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {AppComponentsModule} from './components/appcomponents/app-components.module';
import {NzSpinModule} from 'ng-zorro-antd/spin';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {NzSwitchModule} from 'ng-zorro-antd/switch';

const antdModule = [
  NzButtonModule,
  NzTableModule,
  NzDividerModule,
  NzDropDownModule,
  NzInputModule,
  NzPageHeaderModule,
  NzAvatarModule,
  NzEmptyModule,
  NzSelectModule,
  NzGridModule,
  NzFormModule,
  NzSpaceModule,
  NzToolTipModule,
  NzPopconfirmModule,
  NzIconModule,
  NzSpinModule,
  NzTagModule,
  NzSwitchModule,
];

@NgModule({
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    PerfectScrollbarModule,
    SearchPipe,
    NgxWebstorageModule,
    TranslateModule,
    AppComponentsModule,
    ...antdModule
  ],
  imports: [
    RouterModule,
    CommonModule,
    PerfectScrollbarModule,
    NgxWebstorageModule.forRoot(),
    TranslateModule,
    ...antdModule
  ],
  declarations: [
    SearchPipe
  ],
  providers: [
    ThemeConstantService,
    DatePipe,
    // AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ]
})

export class SharedModule {
}
