export interface GenericModelType {
  id: number;
  name: string;
  description: string;
  createdAt?: Date;
  modifiedAt?: Date;
  createdBy?: string;
  modifiedBy?: string;
  status?: boolean;
}
