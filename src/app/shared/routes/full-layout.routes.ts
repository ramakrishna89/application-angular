import {Routes} from '@angular/router';
import {APP_NAVIGATION_CONSTANTS} from '../constants/app.constant';

export const FULL_LAYOUT_ROUTES: Routes = [
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_AUTHENTICATION,
    loadChildren: () => import('../../authentication/authentication.module').then(m => m.AuthenticationModule)
  }
];
