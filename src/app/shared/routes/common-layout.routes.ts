import {Routes} from '@angular/router';
import {APP_NAVIGATION_CONSTANTS} from '../constants/app.constant';

export const COMMON_LAYOUT_ROUTES: Routes = [
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_HOME,
    loadChildren: () => import('../../home/home.module').then(m => m.HomeModule),
    data: {
      headerDisplay: 'none',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ADMIN_DASHBOARD,
    loadChildren: () => import('../../administration/dashboard/dashboard.module').then(m => m.DashboardModule),
    data: {
      headerDisplay: 'none',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_MENU,
    loadChildren: () => import('../../administration/menu/menu.module').then(m => m.MenuModule),
    data: {
      title: 'app.administration-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_PRODUCT,
    loadChildren: () => import('../../administration/product/product.module').then(m => m.ProductModule),
    data: {
      title: 'app.product-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ROLE_DATA,
    loadChildren: () => import('../../administration/role-data/role-data.module').then(m => m.RoleDataModule),
    data: {
      title: 'app.administration-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ROLE_ACCESS,
    loadChildren: () => import('../../administration/role-access/role-access.module').then(m => m.RoleAccessModule),
    data: {
      title: 'app.administration-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_USER,
    loadChildren: () => import('../../administration/user/user.module').then(m => m.UserModule),
    data: {
      title: 'app.administration-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_PASSWORD,
    loadChildren: () => import('../../administration/password/password.module').then(m => m.PasswordModule),
    data: {
      title: 'app.administration-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_CONFIGURATION,
    loadChildren: () => import('../../report/configuration/configuration.module').then(m => m.ConfigurationModule),
    data: {
      title: 'app.report-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_GENERATION,
    loadChildren: () => import('../../report/generation/generation.module').then(m => m.GenerationModule),
    data: {
      title: 'app.report-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_DOWNLOADS,
    loadChildren: () => import('../../downloads/downloads.module').then(m => m.DownloadsModule),
    data: {
      title: 'app.downloads-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_CONTACT,
    loadChildren: () => import('../../alerts/contact/contact.module').then(m => m.ContactModule),
    data: {
      title: 'app.alert-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_GROUP,
    loadChildren: () => import('../../alerts/group/group.module').then(m => m.GroupModule),
    data: {
      title: 'app.alert-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_FREQUENCY,
    loadChildren: () => import('../../alerts/frequency/frequency.module').then(m => m.FrequencyModule),
    data: {
      title: 'app.alert-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
  {
    path: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_JOBS,
    loadChildren: () => import('../../alerts/jobs/jobs.module').then(m => m.JobsModule),
    data: {
      title: 'app.alert-page.name',
      // role: []
    },
    // canLoad: [AuthGuard],
  },
];
