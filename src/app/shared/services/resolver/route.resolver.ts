import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable, of, throwError} from 'rxjs';
import {catchError, mergeMap} from 'rxjs/operators';
import {CrudService} from '../crud.service';
import {APP_NAVIGATION_CONSTANTS} from '../../constants/app.constant';
import {AlertService} from '../alert.service';

@Injectable({
  providedIn: 'root'
})
export class RouteResolver implements Resolve<any> {


  constructor(private crudService: CrudService, private router: Router, private alertService: AlertService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {
    const id: number = route.params?.id;
    const endPoint: string = route.data?.endPoint;
    if (id && endPoint) {
      return this.crudService.read(id, endPoint)
        .pipe(
          mergeMap((object: any) => {
            if (object) {
              return of(object);
            }
          }), catchError(err => {
            this.alertService.handleHttpErrorResp(err, route.data?.pageName);
            return null;
          }));
    }
    this.router.navigate([APP_NAVIGATION_CONSTANTS.ANG.ERROR_URL]);
    return EMPTY;
  }
}
