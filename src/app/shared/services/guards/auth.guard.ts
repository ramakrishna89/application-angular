import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {APP_NAVIGATION_CONSTANTS, ROLE_SYSADMIN} from '../../constants/app.constant';
import {DatePipe} from '@angular/common';
import {SessionService} from '../session.service';
import {Auth} from '../../../administration/user/model/Auth.type';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private datePipe: DatePipe,
              private router: Router, private sessionService: SessionService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkUserAndRoleType(next.data.role);
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkUserAndRoleType(route.data.role);
  }

  checkUserAndRoleType(allowedRoles): boolean {
    try {
      const user: Auth = this.sessionService.currentUserValue;
      // this.logger.info(this.constructor.name, 'checkUserAndRole, Inner: ', user.expiresAt > new Date());
      // if (user.expiresAt > new Date()) {
      for (const role of user.roleAccessDtos) {
        if (role.roleType === ROLE_SYSADMIN) {
          return true;
        } else if (allowedRoles.indexOf(role.roleType) > -1) {
          return true;
        } else {
          return false;
        }
      }
      // } else {
      // this.router.navigate([APP_NAVIGATION_CONSTANTS.ERROR_URL, {msg: 'app.error-page.session-time-out'}]);
      // }
    } catch (err) {
      console.log(this.constructor.name, 'checkUserAndRole', err);
      this.router.navigate([APP_NAVIGATION_CONSTANTS.ERROR_URL]);
    }
  }
}
