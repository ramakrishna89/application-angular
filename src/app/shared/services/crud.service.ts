import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {APIResponseType, IAPIResponse} from '../interfaces/api.response.type';
import {APP_NAVIGATION_CONSTANTS} from '../constants/app.constant';
import {catchError, map} from 'rxjs/operators';
import {IPagination} from '../interfaces/Pagination.type';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ParamType} from '../interfaces/param.type';

@Injectable({
  providedIn: 'root'
})
export class CrudService {


  constructor(private http: HttpClient) {
  }

  public create(dto: any, endpoint: string): Observable<boolean> {
    return this.http.post<IAPIResponse>(endpoint +
      APP_NAVIGATION_CONSTANTS.API.SERVICE_CREATE, dto)
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          return true;
        }
      }), catchError(err => {
        // console.log(this.constructor.name, 'create', err);
        return throwError(err.error);
      }));
  }

  public update(dto: any, endpoint: string): Observable<boolean> {
    // console.log(this.constructor.name, 'update - input', dto, endpoint);
    return this.http.put<IAPIResponse>(endpoint
      + APP_NAVIGATION_CONSTANTS.API.SERVICE_UPDATE, dto)
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          return true;
        }
      }), catchError(err => {
        // console.log(this.constructor.name, 'update', err);
        return throwError(err.error);
      }));
  }

  public readAll(pagination: IPagination, endpoint: string): Observable<IPagination> {
    return this.http.post<IAPIResponse>(endpoint
      + APP_NAVIGATION_CONSTANTS.API.SERVICE_READ_ALL, pagination)
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          return apiResponse.object as IPagination;
        }
      }), catchError(err => {
        // console.log(this.constructor.name, 'readAll', err);
        return throwError(err.error);
      }));
  }

  public read(id: any, endpoint: string): Observable<any> {
    const params = new HttpParams().append('id', id);
    return this.http.get<IAPIResponse>(endpoint
      + APP_NAVIGATION_CONSTANTS.API.SERVICE_READ, {params})
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          return apiResponse.object;
        }
      }), catchError(err => {
        // console.log(this.constructor.name, 'delete', err);
        return throwError(err.error);
      }));
  }

  public getData(endpoint: string, inputParams?: ParamType[]): Observable<any> {
    let params: HttpParams = new HttpParams();
    if (inputParams) {
      for (const param of inputParams) {
        params = params.append(param.key, param.value);
      }
    }
    return this.http.get<IAPIResponse>(endpoint, {params})
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          return apiResponse.object;
        }
      }), catchError(err => {
        // console.log(this.constructor.name, 'delete', err);
        return throwError(err.error);
      }));
  }

  public delete(id: any, endpoint: string): Observable<boolean> {
    const params = new HttpParams().append('id', id);
    return this.http.delete<IAPIResponse>(endpoint
      + APP_NAVIGATION_CONSTANTS.API.SERVICE_DELETE, {params})
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          return true;
        }
      }), catchError(err => {
        // console.log(this.constructor.name, 'delete', err);
        return throwError(err.error);
      }));
  }

  public statusToggle(id: any, endpoint: string, status: any): Observable<boolean> {
    const params = new HttpParams().append('id', id).append('status', status);
    return this.http.get<IAPIResponse>(endpoint
      + APP_NAVIGATION_CONSTANTS.API.SERVICE_STATUS, {params})
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          return true;
        }
      }), catchError(err => {
        // console.log(this.constructor.name, 'delete', err);
        return throwError(err.error);
      }));
  }
}
