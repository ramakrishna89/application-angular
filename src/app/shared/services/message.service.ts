import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private messageCache = new BehaviorSubject<any>(null);

  pushCacheMessage(data: any): void {
    this.messageCache.next(data);
  }

  getCacheMessage(): Observable<any> {
    return this.messageCache.asObservable();
  }

  clearCacheMessage(): void {
    this.messageCache.next(null);
  }

}
