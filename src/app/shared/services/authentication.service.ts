import {APIResponseType, IAPIResponse} from '../interfaces/api.response.type';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {MessageService} from './message.service';
import {SessionService} from './session.service';
import {Observable, throwError} from 'rxjs';
import {Auth} from '../../administration/user/model/Auth.type';
import {User} from '../../administration/user/model/user';
import {APP_NAVIGATION_CONSTANTS} from '../constants/app.constant';


@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {


  constructor(private http: HttpClient,
              private messageService: MessageService, private sessionService: SessionService) {
  }


  public login(email: string, password: string): Observable<boolean> {
    return this.http.post<IAPIResponse>(APP_NAVIGATION_CONSTANTS.API.MODULE_AUTHENTICATION + 'login', {email, password})
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          const user: Auth = apiResponse.object;
          if (user && user.token) {
            this.sessionService.saveUserToSession(user);
          }
          return true;
        }
      }), catchError(err => {
        // this.logger.info(this.constructor.name, 'login', err);
        return throwError(err.error);
      }));
  }

  public adminSignUp(user: User): Observable<boolean> {
    return this.http.post<IAPIResponse>(APP_NAVIGATION_CONSTANTS.API.MODULE_REGISTRATION + 'admin-sign-up', user)
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          this.messageService.pushCacheMessage('app.success.msg-0');
          return true;
        }
      }), catchError(err => {
        // this.logger.error(this.constructor.name, 'signUp', err);
        return throwError(err.error);
      }));
  }

  public forgotPassword(email: string): Observable<boolean> {
    return this.http.post<IAPIResponse>(APP_NAVIGATION_CONSTANTS.API.MODULE_REGISTRATION + 'forgot-password', {email})
      .pipe(map(apiResponse => {
        // this.logger.info(this.constructor.name, 'forgotPassword', apiResponse);
        return apiResponse.status;
      }), catchError(err => {
        // this.logger.info(this.constructor.name, 'forgotPassword', err);
        return throwError(err.error);
      }));
  }

  public checkAdminStatus(): Observable<boolean> {
    return this.http.post<IAPIResponse>(APP_NAVIGATION_CONSTANTS.API.MODULE_AUTHENTICATION + 'check-admin', {})
      .pipe(map(apiResponse => {
        // this.logger.info(this.constructor.name, 'checkAdminStatus', apiResponse);
        return apiResponse.status;
      }), catchError(err => {
        // this.logger.info(this.constructor.name, 'checkAdminStatus', err);
        return throwError(err.error);
      }));
  }

  public refreshToken(): Observable<boolean> {
    return this.http.post<IAPIResponse>(APP_NAVIGATION_CONSTANTS.API.MODULE_AUTHENTICATION + 'refresh-token', {
      email: this.sessionService.currentUserValue.email,
      refreshToken: this.sessionService.currentUserValue.refreshToken
    })
      .pipe(map(apiResponse => {
        // this.logger.info(this.constructor.name, 'refreshToken', apiResponse);
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          const user: Auth = apiResponse.object;
          if (user && user.token) {
            this.sessionService.clearSessionStorage();
            this.sessionService.saveUserToSession(user);
          }
        }
        return apiResponse.status;
      }), catchError(err => {
        // this.logger.info(this.constructor.name, 'refreshToken', err);
        return throwError(err.error);
      }));
  }

  public logout(): void {
    this.sessionService.clearSessionStorage();
  }
}
