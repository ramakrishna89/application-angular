import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {APP_CONSTANTS} from '../constants/app.constant';
import {SessionStorageService} from 'ngx-webstorage';
import {Auth} from '../../administration/user/model/Auth.type';

@Injectable({
  providedIn: 'root',
})
export class SessionService {

  private currentUserSubject: BehaviorSubject<Auth>;
  public currentUser: Observable<Auth>;

  constructor(private sessionStorageService: SessionStorageService) {
    this.currentUserSubject = new BehaviorSubject<Auth>(JSON.parse(this.sessionStorageService.retrieve(APP_CONSTANTS.CURRENT_USER)));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Auth {
    return this.currentUserSubject.value;
  }

  public saveUserToSession(user: Auth): void {
    this.sessionStorageService.store(APP_CONSTANTS.CURRENT_USER, JSON.stringify(user));
    this.currentUserSubject.next(user);
  }

  public clearSessionStorage(): void {
    this.sessionStorageService.clear(APP_CONSTANTS.CURRENT_USER);
    this.currentUserSubject.next(null);
  }

}
