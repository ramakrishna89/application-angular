import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponentService} from '../../components/appcomponents/app-component.service';
import {AlertService} from '../alert.service';
import {CrudService} from '../crud.service';
import {updateFormDirtyAndValueAndValidity, updateFormPristineAndUntouched, updateSuccessStatus} from '../../utils/Utils.functions';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../constants/app.constant';

export abstract class OpsAbstract {

  public object: any | null = null;
  public curdOps: number | 0 = 0;
  public endPoint: string | null = null;
  public pageName: string | null = null;
  public curdForm: FormGroup;
  public isLoading: boolean;


  protected constructor(public fb: FormBuilder,
                        public router: Router,
                        public activatedRoute: ActivatedRoute,
                        public acService: AppComponentService,
                        public alertService: AlertService,
                        public service: CrudService) {
  }

  protected init(): void {
    this.activatedRoute.data.subscribe((data: { curdOps: number, object: any, endPoint: string, pageName: string }) => {
      if (data) {
        if (data.curdOps) {
          this.curdOps = data.curdOps;
        }
        if (data.pageName) {
          this.pageName = data.pageName;
        }
        if (data.endPoint) {
          this.endPoint = data.endPoint;
        }
        if (data.object) {
          this.object = data.object;
        }
      }
    });
    this.isLoading = false;
  }

  abstract customUpdateValidations(): boolean;

  abstract customCreateValidations(): boolean;

  abstract customPostSuccessOps(): void;

  abstract customPostFailureOps(): void;

  createOrUpdate(finalObject?: any): void {
    updateFormDirtyAndValueAndValidity(this.curdForm);
    // console.log(this.curdForm.value);
    if (this.curdForm.valid) {
      this.isLoading = true;
      if (this.curdOps === APP_CURD_CONSTANTS.CREATE) {
        if (this.customCreateValidations()) {
          this.service.create(finalObject ? finalObject : this.curdForm.value, this.endPoint).subscribe(status => {
            this.customPostSuccessOps();
            updateSuccessStatus(this.alertService, status, this.curdForm, false);
          }, error => {
            this.customPostFailureOps();
            this.alertService.handleHttpErrorResp(error, this.pageName);
          });
        }
      } else if (this.curdOps === APP_CURD_CONSTANTS.UPDATE) {
        if (this.customUpdateValidations()) {
          this.service.update(finalObject ? finalObject : this.curdForm.value, this.endPoint).subscribe(status => {
            this.customPostSuccessOps();
            updateSuccessStatus(this.alertService, status, this.curdForm, true);
          }, error => {
            this.customPostFailureOps();
            this.alertService.handleHttpErrorResp(error, this.pageName);
          });
        }
      }
    } else {
      console.log('Invalid Control: ', this.findInvalidControls());
    }
    this.isLoading = false;
  }

  findInvalidControls(): any {
    const invalid = [];
    const controls = this.curdForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  reset(): void {
    updateFormPristineAndUntouched(this.curdForm);
  }

  selectStringCompareValueFn(o1: string, o2: string): boolean {
    return ((o1 && o2) && ('' + o1) === ('' + o2));
  }

}
