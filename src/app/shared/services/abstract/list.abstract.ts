import {IPagination, Pagination} from '../../interfaces/Pagination.type';
import {NzTableQueryParams} from 'ng-zorro-antd/table';
import {CrudService} from '../crud.service';
import {AlertService} from '../alert.service';
import {ActivatedRoute} from '@angular/router';

export abstract class ListAbstract {

  public pagination: IPagination;
  public endPoint: string | null = null;
  public pageName: string | null = null;

  protected constructor(protected activatedRoute: ActivatedRoute, protected service: CrudService, protected alertService: AlertService) {
  }

  protected init(): void {
    this.activatedRoute.data.subscribe(data => {
      if (data) {
        if (data.endPoint) {
          this.endPoint = data.endPoint;
        }
      }
    });
    this.pagination = new Pagination(false, 1, 10, null, '', '',
      null, null, 0);
  }

  public search(): void {
    if (this.pagination.searchField && this.pagination.searchValue) {
      this.pagination.loading = true;
      this.service.readAll(this.pagination, this.endPoint).subscribe(pagination => {
        this.pagination = pagination;
      }, err => {
        this.pagination.loading = false;
      });
    } else {
      this.alertService.info('app.error.msg-10', true);
    }
  }

}
