import {NzNotificationService} from 'ng-zorro-antd/notification';
import {APIResponseType} from '../interfaces/api.response.type';
import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor(private notification: NzNotificationService, private translate: TranslateService) {
  }

  public success(msgKey: string): void {
    this.notification.success(this.translate.instant('app.general.notification.success'), this.translate.instant(msgKey));
  }

  public info(msgKey: string): void {
    this.notification.info(this.translate.instant('app.general.notification.info'), this.translate.instant(msgKey));
  }

  public warn(msgKey: string): void {
    this.notification.warning(this.translate.instant('app.general.notification.warning'), this.translate.instant(msgKey));
  }

  public error(msgKey: string): void {
    this.notification.error(this.translate.instant('app.general.notification.error'), this.translate.instant(msgKey));
  }


  public handleHttpErrorResp(err: any): void {
    if (err.type === APIResponseType.OBJECT) {
      this.notification.error(this.translate.instant('app.general.notification.error'), err.object.message);
    } else if (err.type === APIResponseType.MESSAGE) {
      this.notification.error(this.translate.instant('app.general.notification.error'), this.translate.instant(err.message));
    } else if (err.type === APIResponseType.LIST) {
      let e = '';
      err.object.forEach(element => {
        e = '<li>' + e + this.translate.instant(element.defaultMessage) + '</li>';
      });
      this.notification.error(this.translate.instant('app.general.notification.error'), e);
    } else {
      this.notification.error(this.translate.instant('app.general.notification.error'),
        this.translate.instant('app.error.msg-0'));
    }
  }

}
