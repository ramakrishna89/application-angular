import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor(private datePipe: DatePipe) { }

  info(className: string, methodName: string, data: any): void {
    console.log(this.datePipe.transform(new Date(), 'dd-MM-yyyy hh:mm:ss'), className, methodName, data);
  }

  error(className: string, methodName: string, data: any): void {
    console.error(this.datePipe.transform(new Date(), 'dd-MM-yyyy hh:mm:ss'), className, methodName, data);
  }

  warn(className: string, methodName: string, data: any): void {
    console.warn(this.datePipe.transform(new Date(), 'dd-MM-yyyy hh:mm:ss'), className, methodName, data);
  }

}
