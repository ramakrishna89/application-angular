import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {APIResponseType, IAPIResponse} from '../../interfaces/api.response.type';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {LoggerService} from '../../utils/logger.service';
import {AuthenticationService} from '../../services/authentication.service';
import {SessionService} from '../../services/session.service';
import {IMenu} from '../../../administration/menu/model/menu.type';
import {TranslateService} from '@ngx-translate/core';
import {APP_NAVIGATION_CONSTANTS} from '../../constants/app.constant';


@Injectable({
  providedIn: 'root'
})
export class SideNavService {

  constructor(private authService: AuthenticationService, private http: HttpClient, private logger: LoggerService,
              private sessionService: SessionService, public translate: TranslateService) {
  }

  public loadMenu(): Observable<any[]> {
    return this.http.post<IAPIResponse>(APP_NAVIGATION_CONSTANTS.API.MODULE_ROLE_ACCESS + 'load_menus_of_role',
      this.sessionService.currentUserValue.roleAccessDto.id)
      .pipe(map(apiResponse => {
        if (apiResponse.status && apiResponse.type === APIResponseType.OBJECT) {
          const menuItems: IMenu[] = apiResponse.object;
          menuItems.forEach(menu => {
            menu.name = this.translate.instant('app.' + menu.path?.toLowerCase() + '.name');
            menu.submenu.forEach(subMenu => {
              subMenu.name = this.translate.instant('app.' + subMenu.path?.toLowerCase() + '.name');
            });
          });
          // this.logger.info(this.constructor.name, 'loadMenu', menuItems);
          return menuItems;
        }
      }), catchError(err => {
        this.logger.info(this.constructor.name, 'loadMenu', err);
        return throwError(err.error);
      }));
  }
}
