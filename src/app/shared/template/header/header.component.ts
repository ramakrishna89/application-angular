import {Component, OnInit} from '@angular/core';
import {ThemeConstantService} from '../../services/theme-constant.service';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {APP_NAVIGATION_CONSTANTS} from '../../constants/app.constant';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {

  searchVisible = false;
  quickViewVisible = false;
  isFolded: boolean;
  isExpand: boolean;

  constructor(private themeService: ThemeConstantService, private authService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.themeService.isMenuFoldedChanges.subscribe(isFolded => this.isFolded = isFolded);
    this.themeService.isExpandChanges.subscribe(isExpand => this.isExpand = isExpand);
  }

  toggleFold(): void {
    this.isFolded = !this.isFolded;
    this.themeService.toggleFold(this.isFolded);
  }

  toggleExpand(): void {
    this.isFolded = false;
    this.isExpand = !this.isExpand;
    this.themeService.toggleExpand(this.isExpand);
    this.themeService.toggleFold(this.isFolded);
  }

  searchToggle(): void {
    this.searchVisible = !this.searchVisible;
  }

  quickViewToggle(): void {
    this.quickViewVisible = !this.quickViewVisible;
  }

  redirectHome(): void {
    this.router.navigate([APP_NAVIGATION_CONSTANTS.ANG.MODULE_HOME]);
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate([APP_NAVIGATION_CONSTANTS.ANG.ERROR_URL, {msg: 'app.error-page.user-logged-out'}]);
  }
}
