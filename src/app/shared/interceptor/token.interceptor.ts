import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

import {AuthenticationService} from '../services/authentication.service';
import {LoggerService} from '../utils/logger.service';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {SessionService} from '../services/session.service';
import {APP_NAVIGATION_CONSTANTS} from '../constants/app.constant';
import {Auth} from '../../administration/user/model/Auth.type';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {
  constructor(private sessionService: SessionService, private authService: AuthenticationService, private logger: LoggerService,
              private router: Router) {
  }

  private isRefreshing = false;

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const currentUser: Auth = this.sessionService.currentUserValue;
    if (currentUser && currentUser.token) {
      request = this.addToken(request, currentUser.token);
    }
    // return next.handle(request);
    return next.handle(request)?.pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && error.status === 401) {
        this.logger.info(this.constructor.name, 'intercept - 401 : ', error);
        return this.handle401Error(request, next);
      } else if (error instanceof HttpErrorResponse && error.status === 403) {
        this.logger.info(this.constructor.name, 'intercept - 403 : ', error);
        this.router.navigate([APP_NAVIGATION_CONSTANTS.ANG.ERROR_URL, {msg: 'app.error-page.session-time-out'}]);
      } else {
        this.logger.info(this.constructor.name, 'intercept - Other : ', error);
        return throwError(error);
      }
    }));
  }

  private addToken(request: HttpRequest<any>, token: string): any {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      return this.authService.refreshToken().pipe(
        map((status: boolean) => {
          if (status) {
            this.isRefreshing = false;
            return next.handle(this.addToken(request, this.sessionService.currentUserValue.token));
          }
        }));
    } else {
      return next.handle(this.addToken(request, this.sessionService.currentUserValue.token));
    }
  }
}
