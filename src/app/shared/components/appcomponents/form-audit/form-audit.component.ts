import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-form-audit',
  templateUrl: './form-audit.component.html'
})
export class FormAuditComponent {

  @Input()
  public object: any;

  @Input()
  public curdOps: number;

}
