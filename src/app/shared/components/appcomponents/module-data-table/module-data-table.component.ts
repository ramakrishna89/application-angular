import {Component, Input, OnInit} from '@angular/core';
import {IPagination, Pagination} from '../../../interfaces/Pagination.type';
import {CrudService} from '../../../services/crud.service';
import {AlertService} from '../../../services/alert.service';
import {NzTableQueryParams} from 'ng-zorro-antd/table';

@Component({
  selector: 'app-module-data-table',
  templateUrl: './module-data-table.component.html'
})
export class ModuleDataTableComponent implements OnInit {


  @Input()
  public endPoint: string;

  @Input()
  public pagination: IPagination;

  @Input()
  public resendMailOption: boolean;

  constructor(public service: CrudService, public alertService: AlertService) {
  }

  ngOnInit(): void {
  }

  public refreshList(): void {
    this.pagination.loading = true;
    this.service.readAll(this.pagination, this.endPoint).subscribe(pagination => {
      this.pagination = pagination;
    }, err => {
      this.pagination.loading = false;
    });
  }

  public delete(id: number, curdOps: number): void {
    this.service.delete(id, this.endPoint).subscribe(status => {
      if (status) {
        this.alertService.success('app.success.msg-0', true);
        this.refreshList();
      }
    }, error => {
      this.alertService.handleHttpErrorResp(error, null);
    });
  }

  public resendActivationMail(id: number): void {
    this.service.getData(this.endPoint + 'resend-activation-mail', [{key: 'id', value: id}])
      .subscribe(status => {
        if (status) {
          this.alertService.success('app.success.msg-0', true);
        }
      }, error => {
        this.alertService.handleHttpErrorResp(error, null);
      });
  }

  public statusToggle(id: number, curdOps: number, inStatus: boolean): void {
    this.service.statusToggle(id, this.endPoint, inStatus).subscribe(status => {
      if (status) {
        this.alertService.success('app.success.msg-0', true);
        this.refreshList();
      }
    }, error => {
      this.alertService.handleHttpErrorResp(error, null);
    });
  }


  public onQueryParamsChanged(params: NzTableQueryParams): void {
    this.pagination.pageSize = params.pageSize;
    this.pagination.pageIndex = params.pageIndex;
    const currentSort = params.sort.find(item => item.value !== null);
    this.pagination.sortField = (currentSort && currentSort.key) || null;
    this.pagination.sortOrder = (currentSort && currentSort.value) || null;
    this.pagination.result = [];
    this.refreshList();
  }

}
