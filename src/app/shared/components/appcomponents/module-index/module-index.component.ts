import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-module-index',
  templateUrl: './module-index.component.html'
})
export class ModuleIndexComponent implements OnInit {

  @Input()
  public acRoute: ActivatedRoute;

  @Input()
  public addButtonDisplay = true;

  @Input()
  public listButtonDisplay = true;

  pageName: string;

  ngOnInit(): void {
    this.acRoute.data.subscribe((data: { endPoint: string, pageName: string }) => {
      if (data) {
        this.pageName = data.pageName;
      }
    });
  }

  constructor(private router: Router) {
  }

  redirectToListPage(): void {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate([this.pageName]));
  }


}
