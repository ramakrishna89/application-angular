import {NgModule} from '@angular/core';
import {FormInputComponent} from './form-input/form-input.component';
import {NzFormModule} from 'ng-zorro-antd/form';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {FieldErrorsComponent} from './field-errors/field-errors.component';
import {ValidatorsModule} from 'ngx-validators';
import {ModuleIndexComponent} from './module-index/module-index.component';
import {NzPageHeaderModule} from 'ng-zorro-antd/page-header';
import {NzSpaceModule} from 'ng-zorro-antd/space';
import {NzAvatarModule} from 'ng-zorro-antd/avatar';
import {RouterModule} from '@angular/router';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {FormPasswordComponent} from './form-password/form-password.component';
import {FormEmailComponent} from './form-email/form-email.component';
import {FormAuditComponent} from './form-audit/form-audit.component';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {FormSubmitButtonsComponent} from './form-submit-buttons/form-submit-buttons.component';
import {FormGeneralComponent} from './form-general/form-general.component';
import {ModuleDataTableComponent} from './module-data-table/module-data-table.component';
import {NzTableModule} from 'ng-zorro-antd/table';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {NzEmptyModule} from 'ng-zorro-antd/empty';

const antdModule = [
  NzFormModule,
  NzButtonModule,
  NzInputModule,
  NzSelectModule,
  NzToolTipModule,
  NzPageHeaderModule,
  NzSpaceModule,
  NzAvatarModule,
  NzIconModule,
  NzDividerModule,
  NzTableModule,
  NzPopconfirmModule,
  NzTagModule,
  NzEmptyModule
];

@NgModule({
  declarations: [
    FormInputComponent,
    FormPasswordComponent,
    FormEmailComponent,
    ModuleIndexComponent,
    FieldErrorsComponent,
    FormAuditComponent,
    FormSubmitButtonsComponent,
    FormGeneralComponent,
    ModuleDataTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    ValidatorsModule,
    ...antdModule

  ],
  exports: [
    FormInputComponent,
    ModuleIndexComponent,
    FormPasswordComponent,
    FormEmailComponent,
    FieldErrorsComponent,
    FormAuditComponent,
    FormSubmitButtonsComponent,
    FormGeneralComponent,
    ModuleDataTableComponent
  ]
})
export class AppComponentsModule {
}
