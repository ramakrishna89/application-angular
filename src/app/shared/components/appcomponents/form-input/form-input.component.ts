import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {
  INPUT_COMPONENT_VALIDATION_PROPERTIES,
  INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES, INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES,
  NUMERIC_COMPONENT_VALIDATION_PROPERTIES
} from '../app-component.service';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormInputComponent),
      multi: true
    }
  ]
})
export class FormInputComponent implements ControlValueAccessor, OnInit {

  @Input()
  public parentForm: FormGroup;
  @Input()
  public controlName: string;
  @Input()
  public controlText: string;
  @Input()
  public specialFlag = false;
  @Input()
  public numericFlag = false;
  @Input()
  public optionalFlag = false;
  @Input()
  public hideLabel = false;
  @Input()
  public curdOps: number;
  public requiredIcon: boolean;
  public minLength: number;
  public maxLength: number;
  public pattern: string;
  public patternText: string;
  public value: string;
  public changed: (value: string) => void;
  public touched: () => void;
  public isDisabled: boolean;

  constructor(public translate: TranslateService) {
  }

  ngOnInit(): void {
    if (this.specialFlag) {
      this.requiredIcon = INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.REQUIRED;
      this.minLength = INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH;
      this.maxLength = INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH;
      this.pattern = INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN;
      this.patternText = this.translate.instant('app.error.msg-11',
        {
          field: this.controlText,
          minLength: this.minLength,
          maxLength: this.maxLength,
          pattern: this.translate.instant(INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN_TRANSLATE_KEY),
          regx: this.pattern
        });
    } else if (this.optionalFlag) {
      this.requiredIcon = INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.REQUIRED;
      this.minLength = INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH;
      this.maxLength = INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH;
      this.pattern = INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN;
      this.patternText = this.translate.instant('app.error.msg-11',
        {
          field: this.controlText,
          minLength: this.minLength,
          maxLength: this.maxLength,
          pattern: this.translate.instant(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN_TRANSLATE_KEY),
          regx: this.pattern
        });
    } else if (this.numericFlag) {
      this.requiredIcon = NUMERIC_COMPONENT_VALIDATION_PROPERTIES.REQUIRED;
      this.minLength = NUMERIC_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH;
      this.maxLength = NUMERIC_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH;
      this.patternText = this.translate.instant('app.error.msg-16',
        {
          field: this.controlText,
          minLength: this.minLength,
          maxLength: this.maxLength,
          pattern: this.translate.instant(NUMERIC_COMPONENT_VALIDATION_PROPERTIES.PATTERN_TRANSLATE_KEY),
        });
    } else {
      this.requiredIcon = INPUT_COMPONENT_VALIDATION_PROPERTIES.REQUIRED;
      this.minLength = INPUT_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH;
      this.maxLength = INPUT_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH;
      this.pattern = INPUT_COMPONENT_VALIDATION_PROPERTIES.PATTERN;
      this.patternText = this.translate.instant('app.error.msg-11',
        {
          field: this.controlText,
          minLength: this.minLength,
          maxLength: this.maxLength,
          pattern: this.translate.instant(INPUT_COMPONENT_VALIDATION_PROPERTIES.PATTERN_TRANSLATE_KEY),
          regx: this.pattern
        });
    }
  }

  public onChange(event: Event): void {
    const value: string = (event.target as HTMLInputElement).value;
    this.changed(value);
  }

  public getFormField(): FormControl {
    return this.parentForm?.get(this.controlName) as FormControl;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.changed = fn;
  }

  registerOnTouched(fn: any): void {
    this.touched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }


}
