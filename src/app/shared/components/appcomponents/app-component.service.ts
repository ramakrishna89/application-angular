import {Injectable} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {PasswordValidators, UniversalValidators} from 'ngx-validators';

export enum AppComponentsEnum {
  BASIC = 0,
  BASIC_SPECIAL = 1,
  NUMERIC = 2,
  LONGTEXT = 3,
  EMAIL = 4,
  SELECT = 6,
  BASIC_SPECIAL_OPTIONAL = 7,
  PASSWORD = 8,
  PHONE
}

export const INPUT_COMPONENT_VALIDATION_PROPERTIES = {
  REQUIRED: true,
  MIN_LENGTH: 3,
  MAX_LENGTH: 250,
  PATTERN: '^[a-zA-Z0-9_ ]*$',
  PATTERN_TRANSLATE_KEY: 'app.regxPatterns.basic'
};

export const INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES = {
  REQUIRED: true,
  MIN_LENGTH: 3,
  MAX_LENGTH: 250,
  PATTERN: '^[a-zA-Z0-9_, ./-]*$',
  PATTERN_TRANSLATE_KEY: 'app.regxPatterns.basicSpecial'
};

export const INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES = {
  REQUIRED: false,
  MIN_LENGTH: 3,
  MAX_LENGTH: 250,
  PATTERN: '^[a-zA-Z0-9_, ./-]*$',
  PATTERN_TRANSLATE_KEY: 'app.regxPatterns.basicSpecial'
};

export const NUMERIC_COMPONENT_VALIDATION_PROPERTIES = {
  REQUIRED: true,
  MIN_LENGTH: 1,
  MAX_LENGTH: 100,
  PATTERN_TRANSLATE_KEY: 'app.regxPatterns.numeric'
};

export const PASSWORD_COMPONENT_VALIDATION_PROPERTIES = {
  REQUIRED: true,
  MIN_LENGTH: 8,
  MAX_LENGTH: 16,
  PATTERN_TRANSLATE_KEY: 'app.regxPatterns.password'
};

export const EMAIL_COMPONENT_VALIDATION_PROPERTIES = {
  REQUIRED: true,
};

export const PHONE_COMPONENT_VALIDATION_PROPERTIES = {
  REQUIRED: true,
};


@Injectable({
  providedIn: 'root'
})
export class AppComponentService {

  constructor() {
  }

  generateFormControl(type: AppComponentsEnum, value: any, controlOrArray: number = 0): any {
    if (type === AppComponentsEnum.BASIC) {
      if (controlOrArray === 1) {
        return new FormControl(value, [Validators.required,
          Validators.minLength(INPUT_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(INPUT_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          Validators.pattern(INPUT_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]);
      } else {
        return [value, [Validators.required,
          Validators.minLength(INPUT_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(INPUT_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          Validators.pattern(INPUT_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]];
      }
    } else if (type === AppComponentsEnum.BASIC_SPECIAL) {
      if (controlOrArray === 1) {
        return new FormControl(value, [Validators.required,
          Validators.minLength(INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          Validators.pattern(INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]);
      } else {
        return [value, [Validators.required,
          Validators.minLength(INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          Validators.pattern(INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]];
      }
    } else if (type === AppComponentsEnum.NUMERIC) {
      if (controlOrArray === 1) {
        return new FormControl(value, [Validators.required,
          Validators.minLength(NUMERIC_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(NUMERIC_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          UniversalValidators.isNumber]);
      } else {
        return [value, [Validators.required,
          Validators.minLength(NUMERIC_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(NUMERIC_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          UniversalValidators.isNumber]];
      }
    } else if (type === AppComponentsEnum.SELECT) {
      if (controlOrArray === 1) {
        return new FormControl(value, [Validators.required]);
      } else {
        return [value, [Validators.required]];
      }
    } else if (type === AppComponentsEnum.EMAIL) {
      if (controlOrArray === 1) {
        return new FormControl(value, [Validators.required, Validators.email]);
      } else {
        return [value, [Validators.required, Validators.email]];
      }
    } else if (type === AppComponentsEnum.PHONE) {
      if (controlOrArray === 1) {
        return new FormControl(value, [Validators.required, UniversalValidators.isNumber,
          Validators.minLength(8), Validators.maxLength(10)]);
      } else {
        return [value, [Validators.required, UniversalValidators.isNumber,
          Validators.minLength(8), Validators.maxLength(10)]];
      }
    } else if (type === AppComponentsEnum.BASIC_SPECIAL_OPTIONAL) {
      if (controlOrArray === 1) {
        return new FormControl(value, [
          Validators.minLength(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          Validators.pattern(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]);
      } else {
        return [value, [
          Validators.minLength(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
          Validators.pattern(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]];
      }
    } else if (type === AppComponentsEnum.PASSWORD) {
      if (controlOrArray === 1) {
        return new FormControl(value, [Validators.required,
          PasswordValidators.alphabeticalCharacterRule(1),
          PasswordValidators.digitCharacterRule(1),
          PasswordValidators.lowercaseCharacterRule(1),
          PasswordValidators.uppercaseCharacterRule(1),
          PasswordValidators.specialCharacterRule(1),
          Validators.minLength(PASSWORD_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(PASSWORD_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH)]);
      } else {
        return [value, [Validators.required,
          PasswordValidators.alphabeticalCharacterRule(1),
          PasswordValidators.digitCharacterRule(1),
          PasswordValidators.lowercaseCharacterRule(1),
          PasswordValidators.uppercaseCharacterRule(1),
          PasswordValidators.specialCharacterRule(1),
          Validators.minLength(PASSWORD_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
          Validators.maxLength(PASSWORD_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH)]];
      }
    }
  }

}
