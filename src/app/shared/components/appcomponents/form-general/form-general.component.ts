import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  AppComponentService,
  INPUT_COMPONENT_VALIDATION_PROPERTIES,
  INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES
} from '../app-component.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-form-general',
  templateUrl: './form-general.component.html'
})
export class FormGeneralComponent implements OnInit {

  @Input()
  public curdForm: FormGroup;

  @Input()
  public curdOps: number;

  @Input()
  public object: any;

  public id: FormControl;
  public name: FormControl;
  public description: FormControl;
  public status: FormControl;
  private namePattern: string;
  private descriptionPattern: string;


  constructor(public fb: FormBuilder, public acService: AppComponentService, public translate: TranslateService) {
  }

  public getFormField(controlName: string): FormControl {
    return this.curdForm?.get(controlName) as FormControl;
  }

  ngOnInit(): void {
    this.namePattern = this.translate.instant('app.error.msg-11',
      {
        field: this.translate.instant('app.generic-page.field.name'),
        minLength: INPUT_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH,
        maxLength: INPUT_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH,
        pattern: this.translate.instant(INPUT_COMPONENT_VALIDATION_PROPERTIES.PATTERN_TRANSLATE_KEY),
        regx: INPUT_COMPONENT_VALIDATION_PROPERTIES.PATTERN
      });
    this.descriptionPattern = this.translate.instant('app.error.msg-11',
      {
        field: this.translate.instant('app.generic-page.field.description'),
        minLength: INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH,
        maxLength: INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH,
        pattern: this.translate.instant(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN_TRANSLATE_KEY),
        regx: INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN
      });
    this.id = new FormControl(this.object?.id);
    this.curdForm.addControl('id', this.id);
    this.name = new FormControl(this.object?.name, [Validators.required,
      Validators.minLength(INPUT_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
      Validators.maxLength(INPUT_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
      Validators.pattern(INPUT_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]);
    this.curdForm.addControl('name', this.name);
    this.description = new FormControl(this.object?.description, [
      Validators.minLength(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH),
      Validators.maxLength(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH),
      Validators.pattern(INPUT_SPECIAL_OPTIONAL_COMPONENT_VALIDATION_PROPERTIES.PATTERN)]);
    this.curdForm.addControl('description', this.description);
    this.status = new FormControl((this.object?.status), [Validators.required]);
    this.curdForm.addControl('status', this.status);
  }
}
