import {Component, Input} from '@angular/core';
import {updateFormPristineAndUntouched} from '../../../utils/Utils.functions';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-form-submit-buttons',
  templateUrl: './form-submit-buttons.component.html'
})
export class FormSubmitButtonsComponent {

  @Input()
  public curdForm: FormGroup;

  @Input()
  public curdOps: number;

  reset(): void {
    updateFormPristineAndUntouched(this.curdForm);
  }

}
