import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {PASSWORD_COMPONENT_VALIDATION_PROPERTIES} from '../app-component.service';

@Component({
  selector: 'app-form-password',
  templateUrl: './form-password.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormPasswordComponent),
      multi: true
    }
  ]
})
export class FormPasswordComponent implements ControlValueAccessor, OnInit {

  @Input()
  public parentForm: FormGroup;
  @Input()
  public controlName: string;
  @Input()
  public controlText: string;
  @Input()
  public curdOps: number;
  public requiredIcon: boolean;
  public minLength: number;
  public maxLength: number;
  public patternText: string;
  public value: string;
  public changed: (value: string) => void;
  public touched: () => void;
  public isDisabled: boolean;

  constructor(public translate: TranslateService) {
  }

  ngOnInit(): void {
    this.requiredIcon = PASSWORD_COMPONENT_VALIDATION_PROPERTIES.REQUIRED;
    this.minLength = PASSWORD_COMPONENT_VALIDATION_PROPERTIES.MIN_LENGTH;
    this.maxLength = PASSWORD_COMPONENT_VALIDATION_PROPERTIES.MAX_LENGTH;
    this.patternText = this.translate.instant('app.error.msg-16',
      {
        field: this.controlText,
        minLength: this.minLength,
        maxLength: this.maxLength,
        pattern: this.translate.instant(PASSWORD_COMPONENT_VALIDATION_PROPERTIES.PATTERN_TRANSLATE_KEY),
      });
  }

  public onChange(event: Event): void {
    const value: string = (event.target as HTMLInputElement).value;
    this.changed(value);
  }

  public getFormField(): FormControl {
    return this.parentForm?.get(this.controlName) as FormControl;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.changed = fn;
  }

  registerOnTouched(fn: any): void {
    this.touched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }


}
