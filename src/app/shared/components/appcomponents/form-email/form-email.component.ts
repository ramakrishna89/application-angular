import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {
  EMAIL_COMPONENT_VALIDATION_PROPERTIES,
  INPUT_COMPONENT_VALIDATION_PROPERTIES,
  INPUT_SPECIAL_COMPONENT_VALIDATION_PROPERTIES,
  NUMERIC_COMPONENT_VALIDATION_PROPERTIES, PASSWORD_COMPONENT_VALIDATION_PROPERTIES
} from '../app-component.service';

@Component({
  selector: 'app-form-email',
  templateUrl: './form-email.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormEmailComponent),
      multi: true
    }
  ]
})
export class FormEmailComponent implements ControlValueAccessor, OnInit {

  @Input()
  public parentForm: FormGroup;
  @Input()
  public controlName: string;
  @Input()
  public controlText: string;
  @Input()
  public curdOps: number;
  public requiredIcon: boolean;
  public patternText: string;
  public value: string;
  public changed: (value: string) => void;
  public touched: () => void;
  public isDisabled: boolean;

  constructor(public translate: TranslateService) {
  }

  ngOnInit(): void {
    this.requiredIcon = EMAIL_COMPONENT_VALIDATION_PROPERTIES.REQUIRED;
    this.patternText = this.translate.instant('app.error.msg-27');
  }

  public onChange(event: Event): void {
    const value: string = (event.target as HTMLInputElement).value;
    this.changed(value);
  }

  public getFormField(): FormControl {
    return this.parentForm?.get(this.controlName) as FormControl;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.changed = fn;
  }

  registerOnTouched(fn: any): void {
    this.touched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }


}
