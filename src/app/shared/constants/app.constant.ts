import {environment} from '../../../environments/environment';

export const APP_CONSTANTS = {
  CURRENT_USER: 'CURRENT_USER',
  LS_LANG: 'LS_LANGUAGE',
  HEADER_DISPLAY: 'headerDisplay',
  TITLE: 'title',
};
export const ROLE_SYSADMIN = 'SYSADMIN';

export const APP_ROLES_MENU_TYPES = ['ADMIN', 'PG'];

export const APP_PERMISSIONS = ['MAKER', 'CHECKER', 'APPROVER'];

const api = '/api/';
const APP_MODULE_NAMES = {
  MODULE_HOME: 'home-page',
  MODULE_AUTHENTICATION: 'authentication-page',
  MODULE_REGISTRATION: 'registration-page',
  MODULE_ADMIN_DASHBOARD: 'admin-dashboard-page',
  MODULE_MENU: 'menu-page',
  MODULE_PASSWORD: 'password-page',
  MODULE_PRODUCT: 'product-page',
  MODULE_ROLE_ACCESS: 'role-access-page',
  MODULE_ROLE_DATA: 'role-data-page',
  MODULE_USER: 'user-page',
  MODULE_ALERT_CONTACT: 'contact-page',
  MODULE_ALERT_FREQUENCY: 'frequency-page',
  MODULE_ALERT_GROUP: 'group-page',
  MODULE_ALERT_JOBS: 'jobs-page',
  MODULE_REPORT_CONFIGURATION: 'report-configuration-page',
  MODULE_REPORT_GENERATION: 'report-generation-page',
  MODULE_DOWNLOADS: 'downloads-page',
};

export const APP_NAVIGATION_CONSTANTS = {
  ANG: {
    ROUTE_ADD: 'add',
    ROUTE_EDIT: 'edit/:id',
    ROUTE_VIEW: 'view/:id',
    LOGIN_URL: APP_MODULE_NAMES.MODULE_AUTHENTICATION + '/login-1',
    SIGNUP_URL: APP_MODULE_NAMES.MODULE_AUTHENTICATION + '/sign-up-1',
    ERROR_URL: APP_MODULE_NAMES.MODULE_AUTHENTICATION + '/error-1',
    ...APP_MODULE_NAMES
  },
  API: {
    SERVICE_READ_ALL: 'read-all',
    SERVICE_READ: 'read',
    SERVICE_CREATE: 'create',
    SERVICE_UPDATE: 'update',
    SERVICE_DELETE: 'delete',
    SERVICE_STATUS: 'change-status',
    SERVICE_ID_NAME_MAP: 'id-name-map',
    MODULE_AUTHENTICATION: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_AUTHENTICATION + '/',
    MODULE_REGISTRATION: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_REGISTRATION + '/',
    MODULE_ADMIN_DASHBOARD: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_ADMIN_DASHBOARD + '/',
    MODULE_MENU: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_MENU + '/',
    MODULE_PRODUCT: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_PRODUCT + '/',
    MODULE_ROLE_DATA: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_ROLE_DATA + '/',
    MODULE_ROLE_ACCESS: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_ROLE_ACCESS + '/',
    MODULE_USER: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_USER + '/',
    MODULE_PASSWORD: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_PASSWORD + '/',
    MODULE_ALERT_CONTACT: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_ALERT_CONTACT + '/',
    MODULE_ALERT_GROUP: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_ALERT_GROUP + '/',
    MODULE_ALERT_FREQUENCY: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_ALERT_FREQUENCY + '/',
    MODULE_ALERT_JOBS: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_ALERT_JOBS + '/',
    MODULE_REPORT_CONFIGURATION: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_REPORT_CONFIGURATION + '/',
    MODULE_REPORT_GENERATION: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_REPORT_GENERATION + '/',
    MODULE_DOWNLOADS: environment.apiUrl + api + APP_MODULE_NAMES.MODULE_DOWNLOADS + '/',
  }
};

export const APP_CURD_CONSTANTS = {
  CREATE: 1,
  UPDATE: 2,
  DELETE: 3,
  READ: 4
};

