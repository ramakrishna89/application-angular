import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-frequency',
  templateUrl: './frequency.component.html',
  styles: []
})
export class FrequencyComponent implements OnInit {

  constructor(public acRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

}
