import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';
import {FrequencyComponent} from '../index/frequency.component';
import {FrequencyListComponent} from '../list/frequency-list.component';
import {FrequencyOpsComponent} from '../ops/frequency-ops.component';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_FREQUENCY,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_ALERT_FREQUENCY
};

export const routes: Routes = [
  {
    path: '',
    component: FrequencyComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_FREQUENCY + '.name',
      description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_FREQUENCY + '.description',
      icon: 'bars',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: FrequencyListComponent,
        data: {
          title: 'app.buttons.all',
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
        component: FrequencyOpsComponent,
        data: {
          title: 'app.buttons.add',
          curdOps: APP_CURD_CONSTANTS.CREATE,
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
        component: FrequencyOpsComponent,
        data: {
          title: 'app.buttons.view',
          curdOps: APP_CURD_CONSTANTS.READ,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
        component: FrequencyOpsComponent,
        data: {
          title: 'app.buttons.edit',
          curdOps: APP_CURD_CONSTANTS.UPDATE,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrequencyRouteModule {
}
