import {Audit} from '../../../shared/interfaces/audit.type';

export interface Frequency extends Audit {
  name: string;
  description: string;
}
