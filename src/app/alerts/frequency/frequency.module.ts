import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {FrequencyComponent} from './index/frequency.component';
import {FrequencyListComponent} from './list/frequency-list.component';
import {FrequencyOpsComponent} from './ops/frequency-ops.component';
import {FrequencyRouteModule} from './route/frequency-route.module';


@NgModule({
  declarations: [FrequencyComponent, FrequencyListComponent, FrequencyOpsComponent],
  imports: [
    SharedModule,
    FrequencyRouteModule
  ]
})
export class FrequencyModule {
}
