import {Component, OnInit} from '@angular/core';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-jobs-ops',
  templateUrl: './jobs-ops.component.html',
  styles: []
})
export class JobsOpsComponent extends OpsAbstract implements OnInit {

  constructor(public fb: FormBuilder,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              public acService: AppComponentService,
              public alertService: AlertService,
              public service: CrudService,
              public translate: TranslateService
  ) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.curdForm = this.fb.group({
      id: [this.object?.id, []],
      name: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL, this.object?.name),
      description: this.acService.generateFormControl(AppComponentsEnum.BASIC_SPECIAL_OPTIONAL, this.object?.description),
    });
  }

  customCreateValidations(): boolean {
    return true;
  }

  customPostFailureOps(): void {
  }

  customPostSuccessOps(): void {
  }

  customUpdateValidations(): boolean {
    return true;
  }

}
