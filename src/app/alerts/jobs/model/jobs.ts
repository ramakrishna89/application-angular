import {Audit} from '../../../shared/interfaces/audit.type';

export interface Jobs extends Audit {
  name: string;
  description: string;
}
