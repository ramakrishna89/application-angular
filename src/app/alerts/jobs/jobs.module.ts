import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {JobsComponent} from './index/jobs.component';
import {JobsListComponent} from './list/jobs-list.component';
import {JobsOpsComponent} from './ops/jobs-ops.component';
import {JobsRouteModule} from './route/jobs-route.module';


@NgModule({
  declarations: [JobsComponent, JobsListComponent, JobsOpsComponent],
  imports: [
    SharedModule,
    JobsRouteModule
  ]
})
export class JobsModule {
}
