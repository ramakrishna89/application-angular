import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {ContactComponent} from './index/contact.component';
import {ContactListComponent} from './list/contact-list.component';
import {ContactOpsComponent} from './ops/contact-ops.component';
import {ContactRouteModule} from './route/contact-route.module';


@NgModule({
  declarations: [ContactComponent, ContactListComponent, ContactOpsComponent],
  imports: [
    SharedModule,
    ContactRouteModule
  ]
})
export class ContactModule {
}
