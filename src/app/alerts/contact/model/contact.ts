import {Audit} from '../../../shared/interfaces/audit.type';

export interface Contact extends Audit {
  name: string;
  description: string;
}
