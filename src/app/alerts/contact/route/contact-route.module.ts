import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';
import {ContactComponent} from '../index/contact.component';
import {ContactListComponent} from '../list/contact-list.component';
import {ContactOpsComponent} from '../ops/contact-ops.component';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_CONTACT,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_ALERT_CONTACT
};

export const routes: Routes = [
  {
    path: '',
    component: ContactComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_CONTACT + '.name',
      description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_ALERT_CONTACT + '.description',
      icon: 'idcard',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: ContactListComponent,
        data: {
          title: 'app.buttons.all',
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
        component: ContactOpsComponent,
        data: {
          title: 'app.buttons.add',
          curdOps: APP_CURD_CONSTANTS.CREATE,
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
        component: ContactOpsComponent,
        data: {
          title: 'app.buttons.view',
          curdOps: APP_CURD_CONSTANTS.READ,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
        component: ContactOpsComponent,
        data: {
          title: 'app.buttons.edit',
          curdOps: APP_CURD_CONSTANTS.UPDATE,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRouteModule {
}
