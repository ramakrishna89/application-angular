import {Audit} from '../../../shared/interfaces/audit.type';

export interface Group extends Audit {
  name: string;
  description: string;
}
