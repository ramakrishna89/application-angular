import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styles: []
})
export class GroupComponent implements OnInit {

  constructor(public acRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

}
