import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {GroupComponent} from './index/group.component';
import {GroupListComponent} from './list/group-list.component';
import {GroupOpsComponent} from './ops/group-ops.component';
import {GroupRouteModule} from './route/group-route.module';


@NgModule({
  declarations: [GroupComponent, GroupListComponent, GroupOpsComponent],
  imports: [
    SharedModule,
    GroupRouteModule
  ]
})
export class GroupModule {
}
