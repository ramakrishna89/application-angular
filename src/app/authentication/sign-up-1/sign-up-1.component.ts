import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from 'src/app/shared/services/authentication.service';
import {LoggerService} from '../../shared/utils/logger.service';
import {NotifyService} from '../../shared/services/notify.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {User} from '../../administration/user/model/user';
import {UniversalValidators} from 'ngx-validators';
import {APP_NAVIGATION_CONSTANTS} from '../../shared/constants/app.constant';


@Component({
  templateUrl: './sign-up-1.component.html'
})

export class SignUp1Component implements OnInit {

  signUpForm: FormGroup;
  user: User;

  submitForm(): void {
    Object.keys(this.signUpForm.controls).forEach(key => {
      this.signUpForm.get(key).markAsDirty();
      this.signUpForm.get(key).updateValueAndValidity();
    });
    if (this.signUpForm.valid) {
      this.user = this.signUpForm.value;
      this.authenticationService.adminSignUp(this.user).subscribe(status => {
        if (status) {
          // this.logger.info(this.constructor.name, 'submitForm', status);
          this.location.replaceState('/');
          this.route.navigate([APP_NAVIGATION_CONSTANTS.ANG.LOGIN_URL]);
        } else {
          this.logger.info(this.constructor.name, 'submitForm', 'Registration failed');
          this.notifyService.error('app.error.msg-3');
        }
      }, err => {
        this.logger.error(this.constructor.name, 'submitForm - RCVDError: ', err);
        this.notifyService.handleHttpErrorResp(err);
      });
    }
  }

  updateConfirmValidator(): void {
    Promise.resolve().then(() => this.signUpForm.controls.checkPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return {required: true};
    } else if (control.value !== this.signUpForm.controls.password.value) {
      return {confirm: true, error: true};
    }
  };

  constructor(private fb: FormBuilder, private authenticationService: AuthenticationService,
              private logger: LoggerService, private notifyService: NotifyService,
              private route: Router, private location: Location) {
  }

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required]],
      phonePrefix: ['+968', [Validators.required]],
      phone: [null, [Validators.required, UniversalValidators.isNumber, Validators.minLength(8), Validators.maxLength(10)]],
      designation: [null, [Validators.required]],
      lang: ['en', [Validators.required]],
      address: [null, [Validators.required]]
    });
  }
}
