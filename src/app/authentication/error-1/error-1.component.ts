import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {LoggerService} from '../../shared/utils/logger.service';
import {ActivatedRoute, Router} from '@angular/router';
import {APP_NAVIGATION_CONSTANTS} from '../../shared/constants/app.constant';

@Component({
  templateUrl: './error-1.component.html'
})

export class Error1Component implements OnInit {

  messageFlag = false;
  message: any;

  constructor(private authService: AuthenticationService, private logger: LoggerService,
              private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.message = this.activatedRoute.snapshot.paramMap.get('msg');
    // this.logger.info(this.constructor.name, 'ngOnInit', this.message);
    if (this.message) {
      this.messageFlag = true;
    }
    this.logoutUser();
  }

  redirectLogin(): void {
    this.router.navigate([APP_NAVIGATION_CONSTANTS.ANG.LOGIN_URL]);
  }

  logoutUser(): void {
    this.authService.logout();
  }
}
