import {NotifyService} from '../../shared/services/notify.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {LocalStorageService} from 'ngx-webstorage';
import {APP_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../shared/constants/app.constant';
import {Subscription} from 'rxjs';
import {MessageService} from '../../shared/services/message.service';
import {Location} from '@angular/common';
import {Auth} from '../../administration/user/model/Auth.type';

@Component({
  templateUrl: './login-1.component.html'
})

export class Login1Component implements OnInit, OnDestroy {
  loginForm: FormGroup;
  user: Auth;
  private messageSubscription: Subscription;
  isAdmin: boolean;

  submitForm(): void {
    Object.keys(this.loginForm.controls).forEach(key => {
      this.loginForm.get(key).markAsDirty();
      this.loginForm.get(key).updateValueAndValidity();
    });
    if (this.loginForm.valid) {
      this.user = this.loginForm.value;
      this.authService.login(this.user.email, this.user.password)
        .subscribe(status => {
          if (status) {
            this.location.replaceState('/');
            this.route.navigate([APP_NAVIGATION_CONSTANTS.ANG.MODULE_HOME]);
          } else {
            // this.logger.info(this.constructor.name, 'submitForm', 'Login failed');
            this.notifyService.error('app.error.msg-3');
          }
        }, err => {
          // this.logger.info(this.constructor.name, 'submitForm - RCVDError: ', err);
          this.notifyService.handleHttpErrorResp(err);
        });
    }
  }

  confirmForgotPassword(): void {
    this.loginForm.get('email').markAsDirty();
    this.loginForm.get('email').updateValueAndValidity();
    if (this.loginForm.get('email').valid) {
      this.modal.confirm({
        nzTitle: this.translate.instant('app.general.forms.confirm.msg-1'),
        nzContent: this.translate.instant('app.general.forms.confirm.msg-2'),
        nzOkText: this.translate.instant('app.general.buttons.ok'),
        nzCancelText: this.translate.instant('app.general.buttons.cancel'),
        nzOnOk: () => this.handleForgotPassword(this.loginForm.get('email').value)
      });
    }
  }

  handleForgotPassword(email: string): void {
    this.authService.forgotPassword(email).subscribe(status => {
      if (status) {
        this.notifyService.info('app.success.msg-1');
      } else {
        // this.logger.info(this.constructor.name, 'handleForgotPassword', 'Password reset failed: ' + status);
        this.notifyService.error('app.success.msg-1');
      }
    }, err => {
      // this.logger.info(this.constructor.name, 'handleForgotPassword - RCVDError: ', err);
      this.notifyService.handleHttpErrorResp(err);
    });
  }

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private route: Router,
              private notifyService: NotifyService,
              private modal: NzModalService, private translate: TranslateService,
              private localstorage: LocalStorageService, private messageService: MessageService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.isAdmin = false;
    this.loginForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]]
    });
    this.getRegistrationMessage();
    this.getAdminStatus();
  }

  getRegistrationMessage(): void {
    this.messageSubscription = this.messageService.getCacheMessage().subscribe(msg => {
      if (msg) {
        this.notifyService.success(msg);
        this.messageService.clearCacheMessage();
      }
    }, err => {
      // this.logger.info(this.constructor.name, 'MessageSubscription - Error: ', err);
    });
  }

  getAdminStatus(): void {
    this.authService.checkAdminStatus().subscribe(status => {
      this.isAdmin = status;
    }, err => {
      // this.logger.info(this.constructor.name, 'getAdminStatus - Error: ', err);
    });
  }

  useLanguage(language: string): void {
    this.localstorage.store(APP_CONSTANTS.LS_LANG, language);
    this.translate.use(language);
  }

  redirectRegistration(): void {
    this.route.navigate([APP_NAVIGATION_CONSTANTS.ANG.SIGNUP_URL]);
  }

  ngOnDestroy(): void {
    if (this.messageSubscription) {
      this.messageSubscription.unsubscribe();
    }
  }
}
