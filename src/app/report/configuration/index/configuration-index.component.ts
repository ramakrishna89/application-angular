import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({selector: 'app-configuration-index', templateUrl: './configuration-index.component.html', styles: []})
export class ConfigurationIndexComponent implements OnInit {
  constructor(public acRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
  }
}
