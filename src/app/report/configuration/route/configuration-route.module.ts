import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConfigurationIndexComponent} from '../index/configuration-index.component';
import {ConfigurationListComponent} from '../list/configuration-list.component';
import {ConfigurationOpsComponent} from '../ops/configuration-ops.component';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_CONFIGURATION,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_REPORT_CONFIGURATION
};
export const routes: Routes = [{
  path: '',
  component: ConfigurationIndexComponent,
  data: {
    title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_CONFIGURATION + '.name',
    description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_CONFIGURATION + '.description',
    icon: 'menu', ...pageNameAndEndPoint
  },
  children: [{
    path: '',
    component: ConfigurationListComponent,
    data: {title: 'app.buttons.all', ...pageNameAndEndPoint}
  }, {
    path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
    component: ConfigurationOpsComponent,
    data: {title: 'app.buttons.add', curdOps: APP_CURD_CONSTANTS.CREATE, ...pageNameAndEndPoint}
  }, {
    path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
    component: ConfigurationOpsComponent,
    data: {title: 'app.buttons.view', curdOps: APP_CURD_CONSTANTS.READ, ...pageNameAndEndPoint},
    resolve: {object: RouteResolver},
  }, {
    path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
    component: ConfigurationOpsComponent,
    data: {title: 'app.buttons.edit', curdOps: APP_CURD_CONSTANTS.UPDATE, ...pageNameAndEndPoint},
    resolve: {object: RouteResolver},
  }]
}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class ConfigurationRouteModule {
}
