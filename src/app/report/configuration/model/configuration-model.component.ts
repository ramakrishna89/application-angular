import {GenericModelType} from '../../../shared/interfaces/generic-model.type';
import {ConfigurationSearch} from './configuration-search-model.component';

export interface Configuration extends GenericModelType {

  reportType: number;
  selectedProductIds?: number[];
  selectedRoleDataIds?: number[];
  staticQuery: string;
  customIdentifier: string;
  tableName: string;
  columnData: string;
  groupByData: string;
  orderByByData: string;
  reportSearchConfigurationDtos: ConfigurationSearch[];
}
