import {GenericModelType} from '../../../shared/interfaces/generic-model.type';

export interface ConfigurationSearch extends GenericModelType {

  operationType: number;
  fieldType: number;
  dropDownQuery: string;
}
