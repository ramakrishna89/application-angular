import {NgModule} from '@angular/core';
import {ConfigurationIndexComponent} from './index/configuration-index.component';
import {ConfigurationListComponent} from './list/configuration-list.component';
import {ConfigurationOpsComponent} from './ops/configuration-ops.component';
import {ConfigurationRouteModule} from './route/configuration-route.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [ConfigurationIndexComponent, ConfigurationListComponent, ConfigurationOpsComponent],
  imports: [SharedModule, ConfigurationRouteModule]
})
export class ConfigurationModule {
}
