import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {OpsAbstract} from '../../../shared/services/abstract/ops.abstract';
import {AppComponentsEnum, AppComponentService} from '../../../shared/components/appcomponents/app-component.service';
import {AlertService} from '../../../shared/services/alert.service';
import {CrudService} from '../../../shared/services/crud.service';
import {APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {Configuration} from '../model/configuration-model.component';

@Component({selector: 'app-configuration-ops', templateUrl: './configuration-ops.component.html', styles: []})
export class ConfigurationOpsComponent extends OpsAbstract implements OnInit {

  reportType = '1';
  moduleType = '0';
  selectedRoleDataIds?: number[];
  selectedProductIds?: number[];
  private roleDataIdNames: Map<any, any>;
  private productIdNames: Map<any, any>;

  constructor(public fb: FormBuilder, public router: Router, public activatedRoute: ActivatedRoute,
              public acService: AppComponentService, public alertService: AlertService,
              public service: CrudService, public translate: TranslateService) {
    super(fb, router, activatedRoute, acService, alertService, service);
  }

  ngOnInit(): void {
    super.init();
    this.getRoleDataIdNameMapping();
    this.getProductIdNameMapping();
    this.curdForm = this.fb.group({
      reportType: this.acService.generateFormControl(AppComponentsEnum.SELECT, this.object?.reportType),
      selectedProductIds: [this.object?.selectedProductIds, []],
      selectedRoleDataIds: [this.object?.selectedRoleDataIds, []],
      staticQuery: [this.object?.staticQuery, [Validators.required]],
      customIdentifier: this.acService.generateFormControl(AppComponentsEnum.BASIC, this.object?.customIdentifier),
    });
  }

  disableHiddenReportTypeControls(): void {
    this.curdForm.get('customIdentifier').disable();
    this.curdForm.get('staticQuery').disable();
    if (this.reportType === '1') {
      this.curdForm.get('staticQuery').enable();
    } else if (this.reportType === '3') {
      this.curdForm.get('customIdentifier').enable();
    }
  }

  customCreateOrUpdate(): void {
    const configuration: Configuration = this.curdForm.value;
    if (configuration.selectedRoleDataIds && configuration.selectedRoleDataIds.length <= 0) {
      this.alertService.error('app.error.msg-31', true);
    } else {
      this.createOrUpdate(configuration);
    }
  }

  customCreateValidations(): boolean {
    return true;
  }

  customPostFailureOps(): void {
  }

  customPostSuccessOps(): void {
  }

  customUpdateValidations(): boolean {
    return true;
  }

  getProductIdNameMapping(): void {
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_PRODUCT + APP_NAVIGATION_CONSTANTS.API.SERVICE_ID_NAME_MAP)
      .subscribe(productIdNames => {
        this.productIdNames = productIdNames;
      });
  }

  getRoleDataIdNameMapping(): void {
    this.service.getData(APP_NAVIGATION_CONSTANTS.API.MODULE_ROLE_DATA + APP_NAVIGATION_CONSTANTS.API.SERVICE_ID_NAME_MAP)
      .subscribe(roleDataIdNames => {
        this.roleDataIdNames = roleDataIdNames;
      });
  }

}
