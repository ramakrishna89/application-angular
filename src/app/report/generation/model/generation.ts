import {Audit} from '../../../shared/interfaces/audit.type';

export interface Generation extends Audit {
  name: string;
  description: string;
}
