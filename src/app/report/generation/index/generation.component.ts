import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-generation',
  templateUrl: './generation.component.html',
  styles: []
})
export class GenerationComponent implements OnInit {

  constructor(public acRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

}
