import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {GenerationComponent} from './index/generation.component';
import {GenerationListComponent} from './list/generation-list.component';
import {GenerationOpsComponent} from './ops/generation-ops.component';
import {GenerationRouteModule} from './route/generation-route.module';


@NgModule({
  declarations: [GenerationComponent, GenerationListComponent, GenerationOpsComponent],
  imports: [
    SharedModule,
    GenerationRouteModule
  ]
})
export class GenerationModule {
}
