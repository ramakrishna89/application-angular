import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {APP_CURD_CONSTANTS, APP_NAVIGATION_CONSTANTS} from '../../../shared/constants/app.constant';
import {RouteResolver} from '../../../shared/services/resolver/route.resolver';
import {GenerationComponent} from '../index/generation.component';
import {GenerationListComponent} from '../list/generation-list.component';
import {GenerationOpsComponent} from '../ops/generation-ops.component';

const pageNameAndEndPoint = {
  pageName: APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_GENERATION,
  endPoint: APP_NAVIGATION_CONSTANTS.API.MODULE_REPORT_GENERATION
};

export const routes: Routes = [
  {
    path: '',
    component: GenerationComponent,
    data: {
      title: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_GENERATION + '.name',
      description: 'app.' + APP_NAVIGATION_CONSTANTS.ANG.MODULE_REPORT_GENERATION + '.description',
      icon: 'merge-cells',
      ...pageNameAndEndPoint
    },
    children: [
      {
        path: '',
        component: GenerationListComponent,
        data: {
          title: 'app.buttons.all',
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_ADD,
        component: GenerationOpsComponent,
        data: {
          title: 'app.buttons.add',
          curdOps: APP_CURD_CONSTANTS.CREATE,
          ...pageNameAndEndPoint
        }
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_VIEW,
        component: GenerationOpsComponent,
        data: {
          title: 'app.buttons.view',
          curdOps: APP_CURD_CONSTANTS.READ,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      },
      {
        path: APP_NAVIGATION_CONSTANTS.ANG.ROUTE_EDIT,
        component: GenerationOpsComponent,
        data: {
          title: 'app.buttons.edit',
          curdOps: APP_CURD_CONSTANTS.UPDATE,
          ...pageNameAndEndPoint
        },
        resolve: {
          object: RouteResolver
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerationRouteModule {
}
