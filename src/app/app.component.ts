import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LocalStorageService} from 'ngx-webstorage';
import {APP_CONSTANTS} from './shared/constants/app.constant';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(private translate: TranslateService, private localstorage: LocalStorageService) {
    const lang: string = this.localstorage.retrieve(APP_CONSTANTS.LS_LANG);
    if (lang) {
      translate.use(lang);
    }else {
      translate.setDefaultLang('en');
    }
  }
}
